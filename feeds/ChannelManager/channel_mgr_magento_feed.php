<?php

set_time_limit(0);
error_reporting(E_ALL);
ini_set("display_errors", "On");

/*****************************************************************************/
/******************************* SCRIPT SETUP ********************************/
/*****************************************************************************/

// 1) Set path to Mage.php file
require_once "../../app/Mage.php";
Mage::app("default");

// 2) Set company name
define("COMPANY_NAME", "LensExperts");

// 3) Set folder for feed output files. Make sure this ends with a forward slash.
define("SAVE_FEED_LOCATION","../ChannelManager/"); 

// 4) Set name of feed zip file
define("CSV_FILENAME", COMPANY_NAME."_test2_products.csv");
define("ZIP_FILENAME", COMPANY_NAME."_test2_products.zip");

// 5) Set FTP info
define("CHANNEL_MGR_FTP_HOST", "ftp.espsoftware.com");
define("CHANNEL_MGR_FTP_LOGIN", COMPANY_NAME);
define("CHANNEL_MGR_FTP_PASSWORD", "Ak93MX12tEO091XcNBy31KaIDmVMZ97712pMJK");

// 6) Set Domain Name
define("DOMAIN_NAME", "www.lensexperts.com");

// 7) Set Default Values
define("DEFAULT_MANUFACTURER", COMPANY_NAME);
define("DEFAULT_CATEGORY", "Contact Lenses");

// 8) Define Attribute Codes to pull
define("ATTR_MANUFACTURER","manufacturer");
define("ATTR_WHOLESALEPRICE", "cost");
define("ATTR_MPN", "");
define("ATTR_UPC", "gtin");
define("ATTR_WEIGHT", "weight");
define("ATTR_CONDITION", "condition");

// 9) Define unique key to trigger export
define("KEY_CODE", "ASDNKLXCKLASFHOIQWRNLKANSLFKASFHQOWINDNKASLASFLKASNFNQLW");

/*****************************************************************************/
/******************************** METHODS ************************************/
/*****************************************************************************/

function getFeedLine($productId) {
	$product_data = array();
	$product = Mage::getModel("catalog/product");
	$product->load($productId);
	$product_data["DomainName"] = DOMAIN_NAME;
	$product_data["SKU"] = $product->getSku();
	$product_data["Name"] = $product->getName();
	$product_data["SellingPrice"] = textRound($product->getSpecialPrice(),2);
	$product_data["SalePrice"] = textRound($product->getPrice(),2);
	$product_data["Description"] = $product->getDescription();
	$product_data["URL"] = $product->getProductUrl();
	$product_data["ImageURL"] = implode('', array(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA), "catalog/product/", $product->getImage()));
	$product_data["Advertise"] = "1";
	$product_data["YourCategory"] = getProductCategoryName($product);
	$product_data["Manufacturer"] = getAttribute($product, ATTR_MANUFACTURER, DEFAULT_MANUFACTURER);
	$product_data["WholeSalePrice"] = textRound(getAttribute($product, ATTR_WHOLESALEPRICE),2);
	$product_data["ManufacturerPartNumber"] = getAttribute($product, ATTR_MPN);
	$product_data["UPC"] = getAttribute($product, ATTR_UPC);
	$product_data["Weight"] =  textRound(getAttribute($product, ATTR_WEIGHT),2);
	$product_data["Condition"] = getAttribute($product, ATTR_CONDITION);
	$product->clearInstance();
	
	// sanitize data
	$feed_data = array_map("cleanAndEnquote", $product_data);
	// join data
	$feed_line = implode(",", $feed_data)."\r\n";
	return $feed_line;
}

function textRound($val, $precision=0) {
	if (strlen($val) > 0) {
		$val = round($val, $precision);
	}
	return $val;
}

function getAttribute(&$product, $key, $default="") {
	$value = $default;
	if (strlen($key) > 0) {
		$resource = $product->getResource();
		$value = $resource->getAttributeRawValue($product->getId(), $key);
	}
	return $value;
}

function getProductCategoryName($mageProduct) {
	$categoryTree = DEFAULT_CATEGORY;
	$categoryTreeList = Array();
	$categoryIds = Mage::getResourceSingleton("catalog/product")->getCategoryIds($mageProduct);
	foreach($categoryIds as $catId) {
		$cat = Mage::getModel("catalog/category")->load($catId);
		$parentId = $cat->getParentId();
		$currentTree = Array($cat->getName());
		while ($parentId != 0) {
			$parentCategory = Mage::getModel("catalog/category")->load($parentId);
			array_unshift($currentTree, $parentCategory->getName());
			$parentId = $parentCategory->getParentId();
		}
		array_push($categoryTreeList, $currentTree);
		$cat->clearInstance();
	}
	
	if (count($categoryTreeList) > 0) {
		$longestTree = Array();
		foreach($categoryTreeList as $tree) {
			if (count($tree) > count($longestTree)) {
				$longestTree = $tree;
			}
		}
		$categoryTree = implode(" > ", $longestTree);
	}
	return $categoryTree;
}
 
function createZip($files = array(),$destination = "",$overwrite = false) {
	//if the zip file already exists and overwrite is false, return false
	if(file_exists($destination) && !$overwrite) {
		return false;
	}
	
	//vars
	$valid_files = array();
	//if files were passed in...
	if(is_array($files)) {
		//cycle through each file
		foreach($files as $file) {
			//make sure the file exists
			if(file_exists($file)) {
				$valid_files[] = $file;
			}
		}
	}
	
	//if we have good files...
	if(count($valid_files)) {
		//create the archive
		$zip = new ZipArchive();
		if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
			return false;
		}
		//add the files
		foreach($valid_files as $file) {
			$new_filename = substr($file,strrpos($file,"/") + 1);
			$zip->addFile($file,$new_filename);
		}
		
		//close the zip -- done!
		$zip->close();
		
		//check to make sure the file exists
		return file_exists($destination);
	}
	else {
		return false;
	}
}

function cleanAndEnquote($text) {
	// 1) convert � � => a o
	$text = preg_replace("/[áàâãªä]/u"   ,"a",$text);
	$text = preg_replace("/[ÁÀÂÃÄ]/u"     ,"A",$text);
	$text = preg_replace("/[ÍÌÎÏ]/u"       ,"I",$text);
	$text = preg_replace("/[íìîï]/u"       ,"i",$text);
	$text = preg_replace("/[éèêë]/u"       ,"e",$text);
	$text = preg_replace("/[ÉÈÊË]/u"       ,"E",$text);
	$text = preg_replace("/[óòôõºö]/u"   ,"o",$text);
	$text = preg_replace("/[ÓÒÔÕÖ]/u"     ,"O",$text);
	$text = preg_replace("/[úùûü]/u"       ,"u",$text);
	$text = preg_replace("/[ÚÙÛÜ]/u"       ,"U",$text);
	$text = preg_replace("/[’‘‹›‚]/u","'",$text);
	$text = preg_replace("/[“”«»„]/u"  ,'"',$text);
	$text = str_replace("–","-",$text);
	$text = str_replace(" "," ",$text);
	$text = str_replace("ç","c",$text);
	$text = str_replace("Ç","C",$text);
	$text = str_replace("ñ","n",$text);
	$text = str_replace("Ñ","N",$text);
	
	//2) Translation CP1252. &ndash; => -
	$trans = get_html_translation_table(HTML_ENTITIES);
	$trans[chr(130)] = "&sbquo;";    // Single Low-9 Quotation Mark
	$trans[chr(131)] = "&fnof;";    // Latin Small Letter F With Hook
	$trans[chr(132)] = "&bdquo;";    // Double Low-9 Quotation Mark
	$trans[chr(133)] = "&hellip;";    // Horizontal Ellipsis
	$trans[chr(134)] = "&dagger;";    // Dagger
	$trans[chr(135)] = "&Dagger;";    // Double Dagger
	$trans[chr(136)] = "&circ;";    // Modifier Letter Circumflex Accent
	$trans[chr(137)] = "&permil;";    // Per Mille Sign
	$trans[chr(138)] = "&Scaron;";    // Latin Capital Letter S With Caron
	$trans[chr(139)] = "&lsaquo;";    // Single Left-Pointing Angle Quotation Mark
	$trans[chr(140)] = "&OElig;";    // Latin Capital Ligature OE
	$trans[chr(145)] = "&lsquo;";    // Left Single Quotation Mark
	$trans[chr(146)] = "&rsquo;";    // Right Single Quotation Mark
	$trans[chr(147)] = "&ldquo;";    // Left Double Quotation Mark
	$trans[chr(148)] = "&rdquo;";    // Right Double Quotation Mark
	$trans[chr(149)] = "&bull;";    // Bullet
	$trans[chr(150)] = "&ndash;";    // En Dash
	$trans[chr(151)] = "&mdash;";    // Em Dash
	$trans[chr(152)] = "&tilde;";    // Small Tilde
	$trans[chr(153)] = "&trade;";    // Trade Mark Sign
	$trans[chr(154)] = "&scaron;";    // Latin Small Letter S With Caron
	$trans[chr(155)] = "&rsaquo;";    // Single Right-Pointing Angle Quotation Mark
	$trans[chr(156)] = "&oelig;";    // Latin Small Ligature OE
	$trans[chr(159)] = "&Yuml;";    // Latin Capital Letter Y With Diaeresis
	$trans["euro"] = "&euro;";    // euro currency symbol
	ksort($trans);

	foreach ($trans as $k => $v) {
		$text = str_replace($v, $k, $text);
	}

	// 3) remove <p>, <br/> ...
	$text = strip_tags($text);
	
	// 4) &amp; => & &quot; => '
	$text = html_entity_decode($text);
	
	// 5) remove Windows-1252 symbols like "TradeMark", "Euro"...
	$text = preg_replace('/[^(\x20-\x7F)]*/','', $text);
	
	if (preg_match('/[,"\n\r]/',$text) == 1) {
		$text = '"'.ereg_replace('"','""',$text).'"';
	}
	return($text);
}

/*****************************************************************************/
/***************************** PRODUCT EXPORT ********************************/
/*****************************************************************************/

// Ensure a key was provided
if (!isset($_GET["key"])) {
	exit;
}
// Ensure key is correct.
if ($_GET["key"] != KEY_CODE) {
	exit;
}

try {
	$feed_txt_path = SAVE_FEED_LOCATION.CSV_FILENAME;
	$feed_zip_path = SAVE_FEED_LOCATION.ZIP_FILENAME;
	$handle = fopen($feed_txt_path, "w");
	$heading = Array("DomainName", "SKU", "Name", "SellingPrice", "SalePrice", 
					 "Description", "URL", "ImageURL", "Advertise", "YourCategory", 
					 "Manufacturer", "WholeSalePrice", "ManufacturerPartNumber", 
					 "UPC", "Weight", "Condition");
	
	fwrite($handle, implode(",", $heading)."\r\n");
	
	$products = Mage::getModel("catalog/product")->getCollection();
	$products->addAttributeToFilter("status", 1); // enabled
	$products->addAttributeToFilter("visibility", 4); // catalog, search
	$products->addAttributeToSelect("*");
	$prodIds = $products->getAllIds();
	
	foreach($prodIds as $productId) {
		fwrite($handle, getFeedLine($productId));
		fflush($handle);
	}
	fclose($handle);
	
	// zip up the feed file
	$files_to_zip = array($feed_txt_path);
	
	//if true, good; if false, zip creation failed
	$result = createZip($files_to_zip, $feed_zip_path, true);
	
	if ($result == false) {
		die("Could not create zip archive.");
	}
	else {
		// ftp to channel mgr
		$connection = ftp_connect(CHANNEL_MGR_FTP_HOST);
		$login = ftp_login($connection, CHANNEL_MGR_FTP_LOGIN, CHANNEL_MGR_FTP_PASSWORD);
		if (!$connection || !$login) { die("Connection attempt failed!"); }
		
		// turn passive mode on
		ftp_pasv($connection, true);
		
		$upload = ftp_put($connection, ZIP_FILENAME, $feed_zip_path, FTP_BINARY);
		
		if (!$upload) { echo "FTP upload failed."; }
		else { echo "FTP upload succeeded.";  }
		
		ftp_close($connection); 
	}
}
catch(Exception $e){
	die($e->getMessage());
}

?>