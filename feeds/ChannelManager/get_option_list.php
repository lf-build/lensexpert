<!DOCTYPE html>
<html>
<body>
<pre>
<?php
set_time_limit(0);
error_reporting(E_ALL);
ini_set('display_errors', 'On');

/*****************************************************************************/
/******************************* SCRIPT SETUP ********************************/
/*****************************************************************************/

// 1) Set path to Mage.php file
require_once '../../app/Mage.php';
Mage::app('default');

$ACCEPTED_OPTIONS = Array("Colors");

/*****************************************************************************/
/**************************** PRODUCT ATTRIBUTES *****************************/
/*****************************************************************************/

function getAttribute($product, $key, $default="") {
	$value = $default;
	if (strlen($key) > 0) {
		$value = $product->getResource()->getAttribute($key)->getFrontend()->getValue($product);
	}
	return $value;
}

$index = isset($_GET['index']) ? $_GET['index'] : 0;
$products = Mage::getModel('catalog/product')->getCollection();
$prodIds = $products->getAllIds();
$product = Mage::getModel('catalog/product');

$i = 0;
foreach ($prodIds as $id) {
	if ($i < 2000) {
		$product->load($id);
		#echo $i.": ";
		if (isset($ACCEPTED_OPTIONS)) {
			if (count($ACCEPTED_OPTIONS) > 0) {
				if (getAttribute($product, "has_options", "0") == "1") {
					#echo "Has Options: ";
					foreach ($product->getOptions() as $o) {
						$data = $o->getData();
						echo $data["title"]."\n";
					}
					#echo " || ".$product->getProductUrl();
				}
			}
		}
		$product->clearInstance();
		#echo "\n";
		flush();
	}
	$i += 1;
}

?>
</pre>
</body>
</html>