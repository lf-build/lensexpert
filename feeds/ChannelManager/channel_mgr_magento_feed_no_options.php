<pre>
<?php

/*****************************************************************************/
/******************************* SCRIPT SETUP ********************************/
/*****************************************************************************/

function exceptions_error_handler($severity, $message, $filename, $lineno) { 
    throw new ErrorException($message, 0, $severity, $filename, $lineno); 
}
set_error_handler('exceptions_error_handler');

// 1) Set path to Mage.php file
require_once "../../app/Mage.php";
Mage::app("default");
Mage::setIsDeveloperMode(true);

// 1.5)
ignore_user_abort(1);
error_reporting(E_ALL);
ini_set('display_errors', 'On');
ini_set('max_execution_time', 18000);
ini_set('default_socket_timeout', 18000);
ini_set('gc_maxlifetime', 18000);
ini_set('memory_limit', '-1');
set_time_limit(0);
// ini_set('memory_limit', '2048M');
// ini_set('mssql.timeout', 18000);
// set_time_limit(18000);

// 2) Set company name
define("COMPANY_NAME", "LensExperts");

// 3) Set folder for feed output files. Make sure this ends with a forward slash.
define("SAVE_FEED_LOCATION","../ChannelManager/"); 

// 4) Set name of feed zip file
define("CSV_FILENAME", COMPANY_NAME."_products.csv");
define("ZIP_FILENAME", COMPANY_NAME."_products.zip");

// 5) Set FTP info
define("CHANNEL_MGR_FTP_HOST", "ftp.espsoftware.com");
define("CHANNEL_MGR_FTP_LOGIN", COMPANY_NAME);
define("CHANNEL_MGR_FTP_PASSWORD", "Xjb84nsdlkfnalas124sdg");

// 6) Set Domain Name
define("DOMAIN_NAME", "www.lensexperts.com");

// 7) Set Default Values
define("DEFAULT_MANUFACTURER", "Lens Experts");
define("DEFAULT_CATEGORY", "Default Category");

// 8) Define Attribute Codes to pull
define("ATTR_MANUFACTURER","manufacturer");
define("ATTR_WHOLESALEPRICE", "");
define("ATTR_MPN", "");
define("ATTR_UPC", "gtin");
define("ATTR_WEIGHT", "");
define("ATTR_CONDITION", "");
define("ATTR_DISABLED", "");

// 9) Define unique key to trigger export
define("KEY_CODE", "ASDNKLXCKLASFHOIQWRNLKANSLFKASFHQOWINDNKASLASFLKASNFNQLW");

// Options with no values: 

// 10) Define accepted OptionTypes
$ADDED_HEADER_OPTIONS = Array();
$ACCEPTED_OPTIONS = Array();

/*****************************************************************************/
/******************************** METHODS ************************************/
/*****************************************************************************/

// function getProductURL($product, $productId) {
//     $rewrite = Mage::getModel("core/url_rewrite");
//     $params = array();
// 	$params['_current'] = false;
//     $parent_ids = Mage::getSingleton("catalog/product_type_configurable")->getParentIdsByChild($productId);
//     $parentId = (count($parent_ids) > 0 ? $parent_ids[0] : null);
//     $product_url = $product->getProductUrl();
//     $id_path = "product/".$parentId;
//     $rewrite->loadByIdPath($id_path);
//     $parent_url = Mage::getUrl($rewrite->getRequestPath(), $params);
//     return $parent_url ? $parent_url : $product_url;
// }


function getProductURL($product, $product_id) {
    $parent_ids = Mage::getModel("catalog/product_type_grouped")->getParentIdsByChild($product_id);
    $product_url = $product->getProductUrl();
    $parent_url = '';
    foreach($parent_ids as $parent_id) {
      $group_product = Mage::getModel("catalog/product")->load($parent_id);
      $group_path = $group_product->getProductUrl();
      $parent_url = $group_path ? $group_path : $parent_url;
    }
    $url = $parent_url ? $parent_url : $product_url;
    $url = preg_replace('/\?.*/', '', $url);
    return $url;
}

function getImageURL($product, $product_id) {
    $image_url = $product->getImage();
    if ($image_url == "no_selection") {
        $parent_ids = Mage::getModel("catalog/product_type_grouped")->getParentIdsByChild($product_id);
        foreach($parent_ids as $parent_id) {
          $group_product = Mage::getModel("catalog/product")->load($parent_id);
          $parent_img = $group_product->getImage();
          $image_url = ($parent_img && $parent_img != "no_selection") ? $parent_img : $image_url;
        }
    }
    return implode('', array(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA), "catalog/product/", $image_url));
}

function getFeedLine($productId, $accepted_options, $added_options) {
	// $product_data["URL"] = implode('', array(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB), $product->getUrlPath()));
    // $product_data["URL"] = $product->getProductUrl();
    // $product_data["URL"] = Mage::getResourceSingleton('catalog/product')->getAttributeRawValue($productId, 'url_key', Mage::app()->getStore());
    // $product_data["YourCategory"] = getProductCategoryName($product);
	// $product_data["ImageURL"] = implode('', array(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA), "catalog/product/", $product->getImage()));

	$product_data = array();
	$prod_model = Mage::getModel("catalog/product");
	$product = $prod_model->load($productId);

	$product_data["DomainName"] = DOMAIN_NAME;
	$product_data["SKU"] = getAttribute($product, "entity_id");
	$product_data["Name"] = $product->getName();
	$product_data["SellingPrice"] = textRound($product->getPrice(),2);
	$product_data["SalePrice"] = textRound($product->getSpecialPrice(),2);
	$product_data["Description"] = $product->getDescription();
    $product_data["URL"] = getProductURL($product, $productId);
    $product_data["ImageURL"] = getImageURL($product, $productId);
	$product_data["Advertise"] = "1";
	$product_data["YourCategory"] = "Contact Lenses";
	$product_data["Manufacturer"] = getAttribute($product, ATTR_MANUFACTURER, DEFAULT_MANUFACTURER);
	$product_data["WholeSalePrice"] = textRound(getAttribute($product, ATTR_WHOLESALEPRICE),2);
	$product_data["ManufacturerPartNumber"] = getAttribute($product, ATTR_MPN);
	$product_data["UPC"] = getAttribute($product, ATTR_UPC);
	$product_data["Weight"] = textRound(getAttribute($product, ATTR_WEIGHT),2);
	$product_data["Condition"] = getAttribute($product, ATTR_CONDITION);
	$product_data["Parentage"] = "StandAlone";
	$product_data["ParentSKU"] = "";
    $product_data["Disabled"] = getAttribute($product, ATTR_DISABLED);
	
	foreach($added_options as $opt) {
		$product_data[$opt] = "";
	}
	
	$filterAcceptedOptions = function($o) use ($accepted_options) {
		$data = $o->getData();
		$val = isset($accepted_options[$data["title"]]);
		return $val;
	};
	
	$children = Array();
	if (isset($accepted_options)) {
		$p_options = $product->getOptions();
		$options = array_filter($p_options, $filterAcceptedOptions);
		if (count($options) > 0) {
			$product_data["Parentage"] = "Parent";
			asort($options);
			$var_set = getVariationSet($options);
			foreach ($var_set as $var) {
				$var_data = $product_data; // Copy Parent Data
				$var_data["Parentage"] = "Child";
				$var_data["ParentSKU"] = $product_data["SKU"];
				foreach ($var as $v) {
					$var_data["SKU"].= "__".$v["id"];
					$var_data["SellingPrice"] += $v["price"];
					if (isset($var_data["SalePrice"])) {
						$var_data["SalePrice"] += $v["price"];
					}
					foreach ($added_options as $opt) {
						if ($accepted_options[$v["type"]] == $opt) {
							$var_data[$opt] = $v["val"];
						}
					}
				}
				$children[]= array_map("cleanAndEnquote", $var_data);
			}
		}
	}
	
	$prod_model->clearInstance();
	$product->clearInstance();
	
	// sanitize data
	$feed_data = array_map("cleanAndEnquote", $product_data);
	
	// join data
	$feed_line = implode(",", $feed_data)."\r\n";
	
	foreach ($children as $child) {
		$feed_line.= implode(",", $child)."\r\n";
	}
	
	$product = null;
	$product_data = null;
	$children = null;
	$feed_data = null;
	$var_data = null;
	$options = null;
	
	return $feed_line;
}

function textRound($val, $precision=0) {
	if (strlen($val) > 0) {
		$val = round($val, $precision);
	}
	return $val;
}

function getAttribute(&$product, $key, $default="") {
	$value = $default;
	if (strlen($key) > 0) {
		$resource = $product->getResource();
		$value = $resource->getAttributeRawValue($product->getId(), $key, Mage::app()->getStore());
	}
	return $value;
}

// function getAttribute(&$product, $key, $default="") {
	// $value = $default;
	// if (strlen($key) > 0) {
		// $resource = $product->getResource();
		// $attr = $resource->getAttribute($key);
		// $front_end = $attr->getFrontend();
		// $value = $front_end->getValue($product);
		// $attr->clearInstance();
	// }
	// return $value;
// }

function getVariant($matrix, $i) {
	$variant = Array();
	foreach ($matrix as $col) {
		$values = $col["values"];
		$count = count($values);
		if ($count > 0) {
			$idx = $i % $count;
			$keys = array_keys($values);
			$key = $keys[$idx];
			$value = $values[$key];
			$val_data = $value->getData();
			$variant[]= Array(
				"type"=>$col["data"]["title"],
				"id"=>$key,
				"sku"=>$val_data["sku"],
				"val"=>$val_data["title"],
				"price"=>$val_data["price"]
			);
			$i = $i / $count;
			$value->clearInstance();
			// $val_data->clearInstance();
		}
	}
	return $variant;
}

function getVariationSet($options) {
	$matrix = Array();
	$permutation_count = 1;
	foreach ($options as $o) {
		$values = $o->getValues();
		$data = $o->getData();
		$matrix[]= Array(
			"values"=>$values, "data"=>$data
		);
		$val_count = count($values);
		if ($val_count > 0) {
			$permutation_count*= $val_count;
		}
	}
	$set = Array();
	for ($i = 0; $i < $permutation_count; $i++) {
		$var = getVariant($matrix, $i);
		$set[]= $var;
	}
	return $set;
}

function getProductCategoryName(&$mageProduct) {
	$catModel = Mage::getModel("catalog/category");
	$categoryTree = DEFAULT_CATEGORY;
	$categoryTreeList = Array();
	$categoryIds = $mageProduct->getcategoryIds();
	if (count($categoryIds) > 0) {
		if (count($categoryIds) < 20) {
			foreach($categoryIds as $catId) {
				$cat = $catModel->load($catId);
				$currentTree = Array($cat->getName());
				$parentId = $cat->getParentId();
				$cat->clearInstance();
				while ((is_null($parentId) == false) && ($parentId > 0)) {
					$lastId = $parentId;
					$parentCategory = $catModel->load($parentId);
					array_unshift($currentTree, $parentCategory->getName());
					$parentId = $parentCategory->getParentId();
					$parentCategory->clearInstance();
					if ($lastId == $parentId) {
						break;
					}
				}
				array_push($categoryTreeList, $currentTree);
				unset($currentTree);
			}
		}
	}
	if (count($categoryTreeList) > 0) {
		$longestTree = Array();
		foreach($categoryTreeList as $tree) {
			if (count($tree) > count($longestTree)) {
				$longestTree = $tree;
			}
		}
		$categoryTree = implode(" > ", $longestTree);
	}
	$catModel->clearInstance();
	return $categoryTree;
}
 
function createZip($files = array(),$destination = "",$overwrite = false) {
	//if the zip file already exists and overwrite is false, return false
	if(file_exists($destination) && !$overwrite) {
		return false;
	}
	
	//vars
	$valid_files = array();
	//if files were passed in...
	if(is_array($files)) {
		//cycle through each file
		foreach($files as $file) {
			//make sure the file exists
			if(file_exists($file)) {
				$valid_files[] = $file;
			}
		}
	}
	
	//if we have good files...
	if(count($valid_files)) {
		//create the archive
		$zip = new ZipArchive();
		if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
			return false;
		}
		//add the files
		foreach($valid_files as $file) {
			$new_filename = substr($file,strrpos($file,"/") + 1);
			$zip->addFile($file,$new_filename);
		}
		
		//close the zip -- done!
		$zip->close();
		
		//check to make sure the file exists
		return file_exists($destination);
	}
	else {
		return false;
	}
}

function cleanAndEnquote($text) {
	// 1) convert � � => a o
	$text = preg_replace("/[áàâãªä]/u"    ,"a",$text);
	$text = preg_replace("/[ÁÀÂÃÄ]/u"      ,"A",$text);
	$text = preg_replace("/[ÍÌÎÏ]/u"        ,"I",$text);
	$text = preg_replace("/[íìîï]/u"        ,"i",$text);
	$text = preg_replace("/[éèêë]/u"        ,"e",$text);
	$text = preg_replace("/[ÉÈÊË]/u"        ,"E",$text);
	$text = preg_replace("/[óòôõºö]/u"    ,"o",$text);
	$text = preg_replace("/[ÓÒÔÕÖ]/u"      ,"O",$text);
	$text = preg_replace("/[úùûü]/u"        ,"u",$text);
	$text = preg_replace("/[ÚÙÛÜ]/u"        ,"U",$text);
	$text = preg_replace("/[’‘‹›‚]/u" ,"'",$text);
	$text = preg_replace("/[“”«»„]/u"   ,'"',$text);
	$text = str_replace("–","-",$text);
	$text = str_replace(" " ," ",$text);
	$text = str_replace("ç","c",$text);
	$text = str_replace("Ç","C",$text);
	$text = str_replace("ñ","n",$text);
	$text = str_replace("Ñ","N",$text);
	
	//2) Translation CP1252. &ndash; => -
	$trans = get_html_translation_table(HTML_ENTITIES);
	$trans[chr(130)] = "&sbquo;";    // Single Low-9 Quotation Mark
	$trans[chr(131)] = "&fnof;";     // Latin Small Letter F With Hook
	$trans[chr(132)] = "&bdquo;";    // Double Low-9 Quotation Mark
	$trans[chr(133)] = "&hellip;";   // Horizontal Ellipsis
	$trans[chr(134)] = "&dagger;";   // Dagger
	$trans[chr(135)] = "&Dagger;";   // Double Dagger
	$trans[chr(136)] = "&circ;";     // Modifier Letter Circumflex Accent
	$trans[chr(137)] = "&permil;";   // Per Mille Sign
	$trans[chr(138)] = "&Scaron;";   // Latin Capital Letter S With Caron
	$trans[chr(139)] = "&lsaquo;";   // Single Left-Pointing Angle Quotation Mark
	$trans[chr(140)] = "&OElig;";    // Latin Capital Ligature OE
	$trans[chr(145)] = "&lsquo;";    // Left Single Quotation Mark
	$trans[chr(146)] = "&rsquo;";    // Right Single Quotation Mark
	$trans[chr(147)] = "&ldquo;";    // Left Double Quotation Mark
	$trans[chr(148)] = "&rdquo;";    // Right Double Quotation Mark
	$trans[chr(149)] = "&bull;";     // Bullet
	$trans[chr(150)] = "&ndash;";    // En Dash
	$trans[chr(151)] = "&mdash;";    // Em Dash
	$trans[chr(152)] = "&tilde;";    // Small Tilde
	$trans[chr(153)] = "&trade;";    // Trade Mark Sign
	$trans[chr(154)] = "&scaron;";   // Latin Small Letter S With Caron
	$trans[chr(155)] = "&rsaquo;";   // Single Right-Pointing Angle Quotation Mark
	$trans[chr(156)] = "&oelig;";    // Latin Small Ligature OE
	$trans[chr(159)] = "&Yuml;";     // Latin Capital Letter Y With Diaeresis
	$trans["euro"] = "&euro;";       // euro currency symbol
	ksort($trans);

	foreach ($trans as $k => $v) {
		$text = str_replace($v, $k, $text);
	}
	unset($trans);

	// 3) remove <p>, <br/> ...
	$text = strip_tags($text);
	
	// 4) &amp; => & &quot; => '
	$text = html_entity_decode($text);
	
	// 5) remove Windows-1252 symbols like "TradeMark", "Euro"...
	$text = preg_replace('/[^(\x20-\x7F)]*/','', $text);
	
	if (preg_match('/[,"\n\r]/',$text) == 1) {
		$text = '"'.preg_replace('/"/','""',$text).'"';
	}
	return($text);
}

/*****************************************************************************/
/***************************** PRODUCT EXPORT ********************************/
/*****************************************************************************/

// Ensure a key was provided
if (!isset($_GET["key"])) {
	exit;
}
// Ensure key is correct.
if ($_GET["key"] != KEY_CODE) {
	exit;
}

$step = -1;
$step_count = -1;
if (isset($_GET["step"])) {
	$step = intval($_GET["step"]);
}
if (isset($_GET["step_count"])) {
	$step_count = intval($_GET["step_count"]);
}

try {
	$feed_txt_path = SAVE_FEED_LOCATION.CSV_FILENAME;
	$feed_zip_path = SAVE_FEED_LOCATION.ZIP_FILENAME;
	$feed_prog_path = SAVE_FEED_LOCATION."progress.log";
	
	if (($step == -1 || $step == 1)) {
		$handle = fopen($feed_txt_path, "w");
		$p_handle = fopen($feed_prog_path, "w");
		$heading = Array("DomainName", "SKU", "Name", "SellingPrice", "SalePrice", 
						 "Description", "URL", "ImageURL", "Advertise", "YourCategory", 
						 "Manufacturer", "WholeSalePrice", "ManufacturerPartNumber", 
						 "UPC", "Weight", "Condition", "Parentage", "ParentSKU", "Disabled");
		if (isset($ADDED_HEADER_OPTIONS)) {
			foreach ($ADDED_HEADER_OPTIONS as $opt) {
				$heading[]= $opt;
			}
		}
		fwrite($handle, implode(",", $heading)."\r\n");
	} else {
		$handle = fopen($feed_txt_path, "a");
	}
	
	$products = Mage::getModel("catalog/product")->getCollection();
	$products->addAttributeToFilter("status", 1); // enabled
	// $products->addAttributeToFilter("visibility", 4); // catalog, search
	$products->addAttributeToSelect("*");
    // $products->addUrlRewrite();
	$prodIds = $products->getAllIds();
	fwrite($p_handle, "Pulled Products.\n");
	arsort($prodIds);
	fwrite($p_handle, "Sorted Products.\n");
	
	fclose(fopen("../ChannelManager/memtest.log", "w"));
	gc_enable();
	$product_count = count($prodIds);
	echo "Found $product_count products.\n";
	$memory_usage = memory_get_usage(true);
	$memory_available = ini_get('memory_limit');
	fwrite($p_handle, "Memory Available: $memory_available\n");
	fwrite($p_handle, "Total Product Count: $product_count | Memory Usage: $memory_usage\n");
	for($i = 0; $i < $product_count; $i++) {
		fwrite($handle, getFeedLine($prodIds[$i], $ACCEPTED_OPTIONS, $ADDED_HEADER_OPTIONS));
		fflush($handle);
	}
	fclose($handle);
	fwrite($p_handle, "Finished writing lines.\n");

	// zip up the feed file
	$files_to_zip = array($feed_txt_path);

	fwrite($p_handle, "Writing file to zip.\n");
	//if true, good; if false, zip creation failed
	$result = createZip($files_to_zip, $feed_zip_path, true);
	
	if ($result == false) {
		die("Could not create zip archive.");
	}
	else {
		fwrite($p_handle, "Wrote file to zip.\n");
		fwrite($p_handle, "FTPing zip.\n");
		// ftp to channel mgr
		$connection = ftp_connect(CHANNEL_MGR_FTP_HOST);
		$login = ftp_login($connection, CHANNEL_MGR_FTP_LOGIN, CHANNEL_MGR_FTP_PASSWORD);
		if (!$connection || !$login) { die("Connection attempt failed!"); }
		
		// turn passive mode on
		ftp_pasv($connection, true);
		
		$upload = ftp_put($connection, ZIP_FILENAME, $feed_zip_path, FTP_BINARY);
		
		if (!$upload) { echo "FTP upload failed."; }
		else { echo "FTP upload succeeded.";  }
		
		ftp_close($connection); 
		fwrite($p_handle, "File FTP'd.\n");
		fclose($p_handle);
	}
}
catch(ErrorException $e) {
	error_log($e->getMessage(), 0, "../ChannelManager/errors.log");
}
catch(Exception $e) {
	error_log($e->getMessage(), 0, "../ChannelManager/feed-errors.log");
}

?>
</pre>