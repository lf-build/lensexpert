<?php

set_time_limit(0);
error_reporting(E_ALL);
ini_set("display_errors", "On");

/*****************************************************************************/
/******************************* SCRIPT SETUP ********************************/
/*****************************************************************************/

// 1) Set path to Mage.php file
require_once "../../app/Mage.php";
Mage::app("default");

// 2) Set company name
define("COMPANY_NAME", "LensExperts");

// 3) Set folder for feed output files. Make sure this ends with a forward slash.
define("SAVE_FEED_LOCATION","../channelmanager/"); 

// 4) Set name of feed zip file
define("CSV_FILENAME", COMPANY_NAME."_sales.csv");
define("ZIP_FILENAME", COMPANY_NAME."_sales.zip");

// 5) Set FTP info
define("CHANNEL_MGR_FTP_HOST", "ftp.espsoftware.com");
define("CHANNEL_MGR_FTP_LOGIN", COMPANY_NAME);
define("CHANNEL_MGR_FTP_PASSWORD", "Xjb84nsdlkfnalas124sdg");

// 6) Set default start date of orders
define("DEFAULT_START_DATE", "2015-03-28");

// 7) Define unique key to trigger export
define("KEY_CODE", "ASDNKLXCKLASFHOIQWRNLKANSLFKASFHQOWINDNKASLASFLKASNFNQLW");

/*****************************************************************************/
/******************************** METHODS ************************************/
/*****************************************************************************/

function enquote($text) {
	if (preg_match('/[,"\n\r]/',$text) == 1) {
		$text = '"'.ereg_replace('"','""',$text).'"';
	}
	return $text;
}

function createZip($files = array(),$destination = "",$overwrite = false) {
	//if the zip file already exists and overwrite is false, return false
	if(file_exists($destination) && !$overwrite) {
		return false;
	}
	
	//vars
	$valid_files = array();
	//if files were passed in...
	if(is_array($files)) {
		//cycle through each file
		foreach($files as $file) {
			//make sure the file exists
			if(file_exists($file)) {
				$valid_files[] = $file;
			}
		}
	}
	
	//if we have good files...
	if(count($valid_files)) {
		//create the archive
		$zip = new ZipArchive();
		if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
			return false;
		}
		//add the files
		foreach($valid_files as $file) {
			$new_filename = substr($file,strrpos($file,"/") + 1);
			$zip->addFile($file,$new_filename);
		}
		
		//close the zip -- done!
		$zip->close();
		
		//check to make sure the file exists
		return file_exists($destination);
	}
	else {
		return false;
	}
}

function getOrderFeedLines($order) {
	$orderFeedLines = "";
	$orderData = array();
	$orderData["OrderId"] = $order->getData("increment_id");
	$orderData["InvoiceID"] = $order->getId();
	$orderData["ClientIP"] = $order->getData("remote_ip");
	$orderData["TrackerID"] = "";
	$orderData["TrackingCode"] = "";
	$orderData["InvoiceDate"] = $order->getCreatedAtStoreDate();
	$orderData["Void"] = 0;
	$orderData["Tracked"] = 1;
	$lineItems = $order->getAllItems();
	foreach ($lineItems as $itemId => $item) {
		$orderData["SKU"] = $item->getSku();
		$orderData["Price"] = round($item->getPrice(),2);
		$orderData["Quantity"] = round($item->getQtyOrdered(),0);
		$orderFeedLines.= implode(array_map("enquote",$orderData),",")."\r\n";
	}
	return $orderFeedLines;
}

/*****************************************************************************/
/******************************* ORDER EXPORT ********************************/
/*****************************************************************************/

// Ensure a key was provided
if (!isset($_GET["key"])) {
	exit;
}
// Ensure key is correct.
if ($_GET["key"] != KEY_CODE) {
	exit;
}

try {
	$start_date = isset($_GET["startdate"]) ? $_GET["startdate"] : DEFAULT_START_DATE;
	$feed_txt_path = SAVE_FEED_LOCATION.CSV_FILENAME;
	$feed_zip_path = SAVE_FEED_LOCATION.ZIP_FILENAME;
	$handle = fopen($feed_txt_path, "w");
	$header = array("OrderId", "InvoiceID", "ClientIP", "TrackerID", 
	                "TrackingCode", "InvoiceDate", "Void", "Tracked",
	                "SKU", "Price", "Quantity");
	$orders = Mage::getModel("sales/order")->getCollection();
	$orders->addFieldToFilter("status", "complete");
	$orders->addFieldToFilter("remote_ip", array("notnull"=>true));
	$orders->addFieldToFilter("created_at", array("gt"=>$start_date));
	
	fwrite($handle, implode(array_map("enquote",$header),",")."\r\n");
	foreach($orders as $order) {
		fwrite($handle, getOrderFeedLines($order));
	}
	fclose($handle);
	
	// zip up the feed file
	$files_to_zip = array($feed_txt_path);
	//if true, good; if false, zip creation failed
	$result = createZip($files_to_zip, $feed_zip_path, true);
	if ($result == false) {
		die("Could not create zip archive.");
	}
	else {
		// ftp to channel mgr
		$connection = ftp_connect(CHANNEL_MGR_FTP_HOST);
		$login = ftp_login($connection, CHANNEL_MGR_FTP_LOGIN, CHANNEL_MGR_FTP_PASSWORD);
		if (!$connection || !$login) {
			die("Connection attempt failed!");
		}
		
		// turn passive mode on
		ftp_pasv($connection, true);

		$upload = ftp_put($connection, ZIP_FILENAME, $feed_zip_path, FTP_BINARY);
		if (!$upload) {
			echo "FTP upload failed.";
		}
		else {
			echo "FTP upload succeeded.";
		}
		
		ftp_close($connection); 
	}
} catch(Exception $e){
	die($e->getMessage());
}
?>