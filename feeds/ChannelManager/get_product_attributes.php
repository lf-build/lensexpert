<!DOCTYPE html>
<html>
<body>
<pre>
<?php
set_time_limit(0);
error_reporting(E_ALL);
ini_set('display_errors', 'On');

/*****************************************************************************/
/******************************* SCRIPT SETUP ********************************/
/*****************************************************************************/

// 1) Set path to Mage.php file
require_once '../../app/Mage.php';
Mage::app('default');

/*****************************************************************************/
/**************************** PRODUCT ATTRIBUTES *****************************/
/*****************************************************************************/

$index = isset($_GET['index']) ? $_GET['index'] : 0;
$products = Mage::getModel('catalog/product')->getCollection();
$prodIds = $products->getAllIds();
$product = Mage::getModel('catalog/product');
$product->load($prodIds[$index]);

$attrs = $product->getAttributes();
foreach($attrs as $attr) {
	$code = $attr->getAttributeCode();
	$value = $attr->getFrontend()->getValue($product);
	echo str_pad($code,30).$value."\n";
}

?>
</pre>
</body>
</html>