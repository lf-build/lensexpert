<?php
class RedPandaPlus_OrderAttachments_Helper_Data extends Mage_Core_Helper_Abstract
{

    const CONFIG_SEND_NOTIFICATION_EMAIL                 = 'redpandaplus_orderattachments/general/send_notification';

    const XML_PATH_EMAIL_GENERAL_IDENTITY                = 'default/redpandaplus_orderattachments/emails/email_identity';
    const XML_PATH_EMAIL_NOTIFICATION_TEMPLATE           = 'order_attachment_notification';
    const XML_PATH_EMAIL_CUSTOMER_NOTIFICATION_TEMPLATE  = 'order_attachment_customer_notification';

    public function getConfiguration()
    {
        $pathGeneral  = 'redpandaplus_orderattachments/order_attachments_general/';
        $pathCheckout = 'redpandaplus_orderattachments/order_attachments_checkout/';
        $pathCustomer = 'redpandaplus_orderattachments/order_attachments_customer/';

        $config = array(
            'attachments_dir'                   => Mage::getStoreConfig($pathGeneral . 'attachments_dir'),
            'allowed_file_extensions'           => Mage::getStoreConfig($pathGeneral . 'allowed_extensions'),
            'num_allowed_attachments'           => Mage::getStoreConfig($pathGeneral . 'num_allowed_attachments'),
            'customer_can_delete_attachments'   => Mage::getStoreConfig($pathCustomer . 'can_delete_attachments'),
            'customer_can_edit_attachments'     => Mage::getStoreConfig($pathCustomer . 'can_edit_attachments'),
            'customer_can_add_new_attachments'  => Mage::getStoreConfig($pathCustomer . 'can_add_new_attachments'),
            'can_add_attachments'               => Mage::getStoreConfig($pathCheckout . 'can_add_attachments'),
            'customer_group_upload_attachments' => Mage::getStoreConfig($pathGeneral . 'customer_group_upload_attachments'),
            'custom_text'                       => Mage::getStoreConfig($pathGeneral . 'custom_text'),
            'max_file_size_attachment'          => Mage::getStoreConfig($pathGeneral . 'max_file_size_attachment'),
            'receive_email_notification'        => Mage::getStoreConfig($pathGeneral . 'receive_email_notification'),
            'send_email_notification_to'        => Mage::getStoreConfig($pathGeneral . 'send_email_notification_to'),
            'notify_customer'                   => Mage::getStoreConfig($pathGeneral . 'notify_customer'),
        );

        return $config;
    }

    public function getAllowedAttachmentSize()
    {
        $config = $this->getConfiguration();
        return $config['max_file_size_attachment'];
    }

    public function isAllowedAttachmentSize($attachmentSize)
    {
        $config = $this->getConfiguration();
        $maxFileSizeAttachment = $config['max_file_size_attachment'];

        if (empty($maxFileSizeAttachment)) return true;

        $attachmentSizeInMB = round($attachmentSize/1048576,2);

        if ($attachmentSizeInMB < $maxFileSizeAttachment) return true;

        return false;
    }

    public function getCustomText()
    {
        $config = $this->getConfiguration();

        return (string) $config['custom_text'];
    }

    public function customerCanUploadAttachment($customer)
    {

        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customerGroupId = 0;
        } else {
            $customerGroupId = $customer->getGroupId();
        }
        $config = $this->getConfiguration();
        if (empty($config['customer_group_upload_attachments'])) return true;

        $allowedCustomerGroups = explode(',', $config['customer_group_upload_attachments']);

        if (in_array($customerGroupId, $allowedCustomerGroups)) return true;

        return false;
    }

    public function getCanAddAttachments()
    {
        $config = $this->getConfiguration();
        return (bool) $config['can_add_attachments'];
    }

    public function getAllowedExtensions()
    {
        $config = $this->getConfiguration();
        if (trim($config['allowed_file_extensions']) == '') return false;

        $allowedExtensions = explode(',',$config['allowed_file_extensions']);
        array_walk($allowedExtensions, create_function('&$val', '$val = trim($val);'));
        return $allowedExtensions;
    }

    public function getCanAddNewAttachments()
    {
        $config = $this->getConfiguration();
        return (bool) $config['customer_can_add_new_attachments'];
    }

    public function getCanEditAttachments()
    {
        $config = $this->getConfiguration();
        return (bool) $config['customer_can_edit_attachments'];
    }

    public function getCanDeleteAttachments()
    {
        $config = $this->getConfiguration();
        
        return (bool) $config['customer_can_delete_attachments'];
    }

    public function getNumberOfAllowedAttachments()
    {
        $config = $this->getConfiguration();
        if ($config['num_allowed_attachments'] === '') return 1000000;
        return (int) $config['num_allowed_attachments'];
    }


    public function getCanSaveAttachments()
    {
        return (bool) ($this->getCanAddNewAttachments() || $this->getCanEditAttachments() || $this->getCanDeleteAttachments());
    }
    
    public function getCanSaveAttachmentsForOrder($order)
    {
        return (bool) ($this->getOrderPermissionStatus($order, 'can_add_new_attachments'))
            || ($this->getOrderPermissionStatus($order, 'can_delete_attachments'))
            || ($this->getOrderPermissionStatus($order, 'can_edit_attachments'));
    }

    public function getFilePath($fileName)
    {
        $config = $this->getConfiguration();

        $filePath = Mage::getBaseDir(). DS . trim($config['attachments_dir'], DS) . DS . trim($fileName, DS);

        return $filePath;
    }

    public function getFileSaveDirPath()
    {
        $config = $this->getConfiguration();

        return Mage::getBaseDir(). DS . trim($config['attachments_dir'], DS);
    }

    public function getFileSaveUrl($fileName)
    {
        $config = $this->getConfiguration();
        return Mage::getStoreConfig('web/unsecure/url'). trim($config['attachments_dir'], DS). DS .trim($fileName, DS);
    }

    public function getContentTypeFromExtension($extension)
	{

		$extension = strtolower($extension);

		$contentType = 'application/octet-stream';

		switch($extension){
    		case 'jpg':
    		case 'jpeg':
    			$contentType = 'image/jpeg';
    		break;
    		case 'gif':
    			$contentType = 'image/gif';
    		break;
    		case 'png':
    			$contentType = 'image/png';
    		break;
    		case 'pdf':
    			$contentType = 'application/pdf';
    		break;
            default:
                $contentType = 'application/octet-stream';
    	}

    	return $contentType;
	}

    /**
     * It is used as a callback method to validate attachments
     *
     * @throws Mage_Core_Exception
     * @param string $filePath (tmp file)
     * @return bool
     */
    public function validateUploadFile($filePath)
    {
        $fileSize = filesize($filePath);

        if (Mage::helper('redpandaplus_orderattachments')->isAllowedAttachmentSize($fileSize)) {
            return true;
        }

        $msg = Mage::helper('redpandaplus_orderattachments')
                                ->__('Your file is too big, please upload a file smaller than %s MB',
                                     Mage::helper('redpandaplus_orderattachments')->getAllowedAttachmentSize()
                            );
        Mage::throwException($msg);
        return false;
    }

    public function sendNotificationEmail($data)
	{

        $storeID = $data['email']['store_id'];

		$translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);

        $result = Mage::getModel('core/email_template')
            ->setDesignConfig(array('area' => 'frontend', 'store' => $storeID));

        $result->sendTransactional(
                self::XML_PATH_EMAIL_NOTIFICATION_TEMPLATE,
                Mage::getConfig()->getNode(self::XML_PATH_EMAIL_GENERAL_IDENTITY),
                $data['to_email'],
                $data['to_name'],
                $data['email'],
                $storeID
               );
        //echo $result->getProcessedTemplate($data);

        $translate->setTranslateInline(true);

        return $result;
	}

    public function getOrderNotificationSendToEmail()
    {
        $configuration = $this->getConfiguration();
        return $configuration['send_email_notification_to'];
    }

    public function canSendEmailNotifications()
    {
        $configuration = $this->getConfiguration();
        return (bool) $configuration['receive_email_notification'];
    }
    
    public function canNotifyCustomer()
    {
        $configuration = $this->getConfiguration();
        return (bool) $configuration['notify_customer'];
    }
    
    /**
     * Get if order permission is set or not
     * @param Mage_Sales_Model_Order $order
     * @param string $permission optional
     *
     * @return boolean
     */
    public function getOrderPermissionStatus($order, $permission)
    {
        $permissions = $order->getData('order_attachment_permissions');
        $permissions = json_decode($permissions, true);
        $permissions = is_array($permissions) ? $permissions : array();
        
        if (!empty($permissions[$permission])) return true;
        
        // use the config permissions if the permission was not yet set for the order
        if (!isset($permissions[$permission])) {
            if ($permission == 'can_add_new_attachments') {
                return $this->getCanAddNewAttachments();
            }
            
            if ($permission == 'can_delete_attachments') {
                return $this->getCanDeleteAttachments();
            }            
            
            if ($permission == 'can_edit_attachments') {
                return $this->getCanEditAttachments();
            } 
        }
        
        return false;
    }
    
    public function sendCustomerNotificationEmail($data)
	{

        $storeID = $data['email']['store_id'];

		$translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);

        $result = Mage::getModel('core/email_template')
            ->setDesignConfig(array('area' => 'frontend', 'store' => $storeID));
            
        $mail = $result->getMail();
        
        $orderAttachments = $data['email']['order_attachments'];
        
        foreach ($orderAttachments as $orderAttachment) {
            $attachment               = $mail->createAttachment(file_get_contents(Mage::helper('redpandaplus_orderattachments')->getFilePath($orderAttachment->getFile())));
            $attachment ->type        = Zend_Mime::TYPE_OCTETSTREAM;
            $attachment ->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
            $attachment ->encoding    = Zend_Mime::ENCODING_BASE64;
            $attachment ->filename    = basename($orderAttachment->getFile());
        }

        $result->sendTransactional(
                self::XML_PATH_EMAIL_CUSTOMER_NOTIFICATION_TEMPLATE,
                Mage::getConfig()->getNode(self::XML_PATH_EMAIL_GENERAL_IDENTITY),
                $data['to_email'],
                $data['to_name'],
                $data['email'],
                $storeID
               );
        //echo $result->getProcessedTemplate($data);

        $translate->setTranslateInline(true);

        return $result;
	}
    
    function toByteSize($p_sFormatted) {
        if (substr($p_sFormatted, -1) == 'M') {
            return substr($p_sFormatted, 0, -1) * pow(1024, 2);
        }
    }

}
