<?php
class RedPandaPlus_OrderAttachments_Model_Observer
{

    /**
     * Observers checkout_type_onepage_save_order_after
     *
     * Adds the order attachments saved on session
     *
     * @param $observer array('order'=>$order, 'quote'=>$this->getQuote())
     *
     */
    public function saveOrderAttachments($observer)
    {
        $order = $observer->getEvent()->getOrder();

        $orderAttachments = Mage::getSingleton('redpandaplus_orderattachments/session')->getOrderAttachments();

        if (empty($orderAttachments) || !is_array($orderAttachments)) return;

        $orderAttachmentsCollection = array();
        foreach($orderAttachments as $orderAttachment) {
            if (empty($orderAttachment['file'])) continue;
            $orderAttachmentModel = Mage::getModel('redpandaplus_orderattachments/orderattachment');
            $orderAttachmentModel->setData('order_id', $order->getId());
            if (isset($orderAttachment['comment'])) {
                $orderAttachmentModel->setData('comment', $orderAttachment['comment']);
            }
            $orderAttachmentModel->setData('visible_customer_account', 1);
            $orderAttachmentModel->setData('related_product_id', '');
            $orderAttachmentModel->setData('file', $orderAttachment['file']);
            $orderAttachmentModel->save();
            $orderAttachmentsCollection[] = $orderAttachmentModel;
        }

        Mage::dispatchEvent('order_attachments_updated', array(
            'order'             => $order,
            'order_attachments' => $orderAttachmentsCollection,
            'updated_by'        => 'customer',
        ));

        Mage::getSingleton('redpandaplus_orderattachments/session')->setOrderAttachments(null);
    }

    /**
     * Observers order_attachments_updated
     *
     * Sends notification email
     *
     * @param $observer array('order'=>$order, 'orderAttachments'=> array, 'updatedBy' => string)
     */
    public function sendNotificationEmailOrderAttachments($observer)
    {
        $order            = $observer->getEvent()->getOrder();
        $orderAttachments = $observer->getEvent()->getOrderAttachments();
        $updatedBy        = $observer->getEvent()->getUpdatedBy();

        if (empty($orderAttachments)) return;        

        // notify the admin
        if ($updatedBy == 'customer') {
            if (!Mage::helper('redpandaplus_orderattachments')->canSendEmailNotifications()) return;
            
            $data['to_email'] = Mage::helper('redpandaplus_orderattachments')->getOrderNotificationSendToEmail();
            $data['to_name']  = Mage::getConfig()
                ->getNode(RedPandaPlus_OrderAttachments_Helper_Data::XML_PATH_EMAIL_GENERAL_IDENTITY);

            $data['email']['order']            = $order;
            $data['email']['order_attachments'] = $orderAttachments;
            $data['email']['store_id']         = $order->getStoreId();
            if (empty($data['to_email'])) return;

            Mage::helper('redpandaplus_orderattachments')->sendNotificationEmail($data);
        } elseif ($updatedBy == 'admin') {
            if (!Mage::helper('redpandaplus_orderattachments')->canNotifyCustomer()) return;
            
            $data['to_email'] = $order->getCustomerEmail();
            $data['to_name']  = $order->getCustomerName();

            $data['email']['order']             = $order;
            $data['email']['order_attachments'] = $orderAttachments;
            $data['email']['store_id']          = $order->getStoreId();
            if (empty($data['to_email'])) return;

            Mage::helper('redpandaplus_orderattachments')->sendCustomerNotificationEmail($data);
        }
    }

}
