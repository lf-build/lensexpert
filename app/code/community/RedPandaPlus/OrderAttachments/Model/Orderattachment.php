<?php
class RedPandaPlus_OrderAttachments_Model_Orderattachment extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('redpandaplus_orderattachments/orderattachment');
    }

    /**
     * overrided to remove the file
     */
    protected function _beforeDelete()
    {
        //remove the file
        $filePath = Mage::helper('redpandaplus_orderattachments')->getFilePath($this->getFile());
        @unlink($filePath);

        parent::_beforeDelete();
    }

    /**
     * overrided to remove the old file
     */
    protected function _beforeSave()
    {
        if ($this->getData('old_file')) {
            $filePath = Mage::helper('redpandaplus_orderattachments')->getFilePath($this->getData('old_file'));
            @unlink($filePath);
        }

        parent::_beforeSave();
    }


    public function getOrderAttachments($orderId)
    {
        $collection = Mage::getModel('redpandaplus_orderattachments/orderattachment')->getCollection()
                        ->addFieldToFilter('order_id', $orderId);

        return $collection->toArray();
    }

    public function getVisibleOrderAttachments($orderId)
    {
        $collection = Mage::getModel('redpandaplus_orderattachments/orderattachment')->getCollection()
                        ->addFieldToFilter('order_id', $orderId)
                        ->addFieldToFilter('visible_customer_account', 1);

        return $collection->toArray();
    }

    public function getOrderAttachmentById($orderId, $orderAttachmentId)
    {
        $collection = Mage::getModel('redpandaplus_orderattachments/orderattachment')->getCollection()
                        ->addFieldToFilter('order_id', $orderId)
                        ->addFieldToFilter('order_attachment_id', $orderAttachmentId);

        if ($collection->getSize()) return $collection->getFirstItem();

        return false;
    }

    public function getFileFromHash($hash)
    {
        return $this->getResource()->getFileFromHash($hash);
    }

    /**
     * This is used to generate a random string, we don't really need the filename
     */
    public static function getHash($file)
    {
        $tocken = 'rpoa';

        $hash = Mage::helper('core')->getHash($tocken.'#'.$file, $salt = 2);

        return $hash;
    }

    /**
     * Bulk save attachments - not used at the moment
     */
    public function saveOrderAttachments($orderId, $data)
    {
        return $this->getResource()->saveOrderAttachments($orderId, $data);
    }


}