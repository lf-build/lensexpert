<?php
class RedPandaPlus_OrderAttachments_Model_Mysql4_Orderattachment_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    public function _construct()
    {
        $this->_init('redpandaplus_orderattachments/orderattachment');
    }


    /**
     * Adds the orders to the collection
     *
     * @return RedPandaPlus_OrderAttachments_Model_Mysql4_Orderattachment_Collection
     */
    public function joinOrders()
    {
    	$ordersTable = Mage::getSingleton('core/resource')->getTableName('sales/order');

        // we also add the store id as a filter because we could have multiple
        // entries for the same category (different stores)
    	$this->getSelect()->joinInner(
        	array('_table_order' => $ordersTable),
            '_table_order.entity_id = main_table.order_id')
            ->from("",array(
                        'customer_id' => "_table_order.customer_id",
                        )
            );

        return $this;

    }


}
