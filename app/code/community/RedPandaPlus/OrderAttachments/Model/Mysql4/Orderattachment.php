<?php
class RedPandaPlus_OrderAttachments_Model_Mysql4_Orderattachment extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('redpandaplus_orderattachments/orderattachment','order_attachment_id');
    }

    /**
     * overrided to set the created_on, updated_on fields, remove old file
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        if (!$object->getCreatedOn()) {
            $object->setCreatedOn($this->formatDate(time()));
            $hash = RedPandaPlus_OrderAttachments_Model_Orderattachment::getHash($object->getFile());
            $object->setHash($hash);
        }
        $object->setUpdatedOn($this->formatDate(time()));


        parent::_beforeSave($object);
    }



    public function getFileFromHash($hash)
    {

        $select = $this->_getReadAdapter()->select()
                        ->from($this->getMainTable())
                        ->where('hash = ?', $hash);
        $result = $this->_getReadAdapter()->fetchRow($select);

        if (!empty($result['file'])) return $result['file'];

        return '';
    }


    /**
     * Bulk attachment saving - not used at the moment
     *
     */
    public function saveOrderAttachments($orderId, $data)
    {
        if (!is_array($data)) return;

        foreach ($data as $recordData) {
            if (!empty($recordData['removed']) && $recordData['removed'] == 1) {
                $filePath = Mage::helper('redpandaplus_orderattachments')->getFilePath($recordData['file_name']);
                @unlink($filePath);
                $this->_getWriteAdapter()->delete($this->getMainTable(),
                    $this->_getWriteAdapter()->quoteInto('order_attachment_id=?', $recordData['order_attachment_id'])
                );
                continue;
            }


            $record = array(
                'order_id' => $orderId,
                'related_product_id' => $recordData['product'],
                'comment' => $recordData['comment'],
                'visible_customer_account' => !empty($recordData['show'])? 1: 0,
                'updated_on' => $this->formatDate(time()),
            );

            // new file was uploaded
            if (!empty($recordData['file'])) {
                $record['file'] = $recordData['file'];
                $record['hash'] = Mage::helper('core')->getHash($recordData['file'], $salt = 2);
            }

            if (!empty($recordData['order_attachment_id'])) {
                $condition = $this->_getWriteAdapter()->quoteInto("order_attachment_id = ?", $recordData['order_attachment_id']);
                $this->_getWriteAdapter()->update($this->getMainTable(),
                                                  $record,
                                                  $condition);
            } else {
                $recordData['created_on'] = $this->formatDate(time());
                $this->_getWriteAdapter()->insert($this->getMainTable(), $record);
            }

        }
    }



}
