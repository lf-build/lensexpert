<?php
class RedPandaPlus_OrderAttachments_Model_Api extends Mage_Api_Model_Resource_Abstract
{

    public function create($orderAttachmentData)
    {
        $files = array();
    
        try {
            
            if (empty($orderAttachmentData['order_id'])) {
                throw new Mage_Core_Exception("Missing order id");
            }
            
            if (empty($orderAttachmentData['file']) || empty($orderAttachmentData['file']['name'])
                || empty($orderAttachmentData['file']['content'])) {
                throw new Mage_Core_Exception("Invalid ");
            }
            
            // save file
            $fileContent    = @base64_decode($orderAttachmentData['file']['content'], true);
            $fileName       = $orderAttachmentData['file']['name'];
            $dispersionPath = Varien_File_Uploader::getDispretionPath($fileName); 
            $saveDirectory  = Mage::helper('redpandaplus_orderattachments')->getFileSaveDirPath() . $dispersionPath;
            
            $ioAdapter = new Varien_Io_File();
            $ioAdapter->setAllowCreateFolders(true);
            $ioAdapter->open(array('path' => $saveDirectory));
            $ioAdapter->streamOpen($saveDirectory . DS . $fileName, 'w+');
            $ioAdapter->streamLock(true);
            $ioAdapter->streamWrite($fileContent);
            $ioAdapter->streamUnlock();
            $ioAdapter->streamClose();
            
            unset($fileContent);
            
            $hash = RedPandaPlus_OrderAttachments_Model_Orderattachment::getHash($fileName);
            
            $orderAttachmentModel = Mage::getModel('redpandaplus_orderattachments/orderattachment');          
            $orderAttachmentModel->setData('hash', $hash);   
            $orderAttachmentModel->setData('order_id', $orderAttachmentData['order_id']);   
            $orderAttachmentModel->setData('file', $dispersionPath . DS. $fileName);
            $orderAttachmentModel->setData('comment', empty($orderAttachmentData['comment']) ? '' : $orderAttachmentData['comment']);
            $orderAttachmentModel->setData('visible_customer_account', empty($orderAttachmentData['visible_customer_account']) ? 0 : 1);
            $orderAttachmentModel->save();
            
            
        } catch (Mage_Core_Exception $e) {            
            $this->_fault('not_created', $e->getMessage());            
        } catch (Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
        }
        
        return $orderAttachmentModel->getData();
    }

    public function items($data)
    {    
        $files = array();
    
        try {
            
            if (empty($data['order_id'])) {
                throw new Mage_Core_Exception("Missing order id");
            }
            
            $orderAttachments = Mage::getModel('redpandaplus_orderattachments/orderattachment')
            ->getOrderAttachments($data['order_id']);            
    
            if (!empty($orderAttachments['items'])) {
                foreach ($orderAttachments['items'] as $item) {
                    $files[] = array(
                        'id'                  => $item['order_attachment_id'],
                        'order_attachment_id' => $item['order_attachment_id'],
                        'order_id'            => $item['order_id'],                       
                        'url'                 => Mage::getUrl('orderattachments/index/getFile', array('file_id' => $item['hash'])),
                        'file'                => $item['file'],
                        'file_name'           => basename($item['file']),
                        'comment'             => Mage::helper('core')->htmlEscape($item['comment']),
                        'visible_customer_account'                => $item['visible_customer_account'],
                        'created_on'          => $item['created_on'],
                        'updated_on'          => $item['updated_on'],
                    );
                }
            }
        } catch (Mage_Core_Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());            
        } catch (Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
        }        

        return $files;
    }

    public function delete($data)
    {
        try {
            
            if (empty($data['order_attachment_id'])) {
                throw new Mage_Core_Exception("Missing order attachment id");
            }
            
            $orderAttachmentModel = Mage::getModel('redpandaplus_orderattachments/orderattachment')->load($data['order_attachment_id']);     
    
            if (!$orderAttachmentModel->getId()) {
                throw new Mage_Core_Exception(sprintf("Invalid order attachment id %s", $data['order_attachment_id']));
            }
            $orderAttachmentModel->delete();
            
        } catch (Mage_Core_Exception $e) {
            $this->_fault('not_deleted', $e->getMessage());            
        } catch (Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
        }
        
        return true;
    }
}