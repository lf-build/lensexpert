<?php
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->run("
ALTER TABLE {$this->getTable('sales_flat_order')} ADD COLUMN order_attachment_permissions text; 
");

$installer->endSetup();
