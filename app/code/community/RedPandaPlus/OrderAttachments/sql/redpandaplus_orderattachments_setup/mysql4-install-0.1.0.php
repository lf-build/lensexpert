<?php
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('rpp_order_attachments')};
CREATE TABLE {$this->getTable('rpp_order_attachments')} (
  `order_attachment_id` int(11) unsigned NOT NULL auto_increment,
  `order_id` int(11) unsigned NOT NULL,
  `related_product_id` int(11) unsigned NOT NULL,
  `file` varchar(255),
  `hash` varchar(255),
  `comment` text,
  `visible_customer_account` tinyint(1),
  `created_on` datetime,
  `updated_on` datetime,
  PRIMARY KEY (`order_attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();
