<?php
class RedPandaPlus_OrderAttachments_Block_Attachments extends Mage_Core_Block_Template
{

    protected $_config;

    protected $_systemConfig;

    protected function _construct()
    {
        parent::_construct();

        $this->_systemConfig = Mage::helper('redpandaplus_orderattachments')->getConfiguration();

        if (!$this->getTemplate()) {
            $this->setTemplate('redpandaplus_orderattachments/checkout/onepage/attachments.phtml');
            $this->setHtmlId('order_attachments');
        }
    }

    public function getBlockUrl()
    {
        return Mage::getUrl('orderattachments/index/getCheckoutAttachments', array('_secure' => true));
    }

    public function getSubmitUrl()
    {
        return Mage::getUrl('orderattachments/index/saveCheckoutAttachments', array('_secure' => true));
    }


    public function getFieldId()
    {
        return 'order-attachment';
    }

    public function getFieldName()
    {
        return 'order_attachment';
    }

    public function getCustomText()
    {
        return Mage::helper('redpandaplus_orderattachments')->getCustomText();
    }

    public function getVisibleOrderAttachments($order)
    {

        $orderAttachments = Mage::getModel('redpandaplus_orderattachments/orderattachment')
                                ->getVisibleOrderAttachments($order->getId());

        $files = array();

        if (!empty($orderAttachments['items'])) {
            foreach ($orderAttachments['items'] as $item) {
                $url = Mage::getUrl('orderattachments/index/getFile', array(
                    '_secure'=>true,
                    'file_id' => $item['hash']
                ));

                $files[] = array(
                    'id'                  => $item['order_attachment_id'],
                    'order_attachment_id' => $item['order_attachment_id'],
                    'order_id'            => $item['order_id'],
                    'product'             => $item['related_product_id'],
                    'url'                 => $url,
                    'file'                => $item['file'],
                    'file_name'           =>  basename($item['file']),
                    'comment'             => $this->htmlEscape($item['comment']),
                    'show'                => $item['visible_customer_account'],
                    'created_on'          => $item['created_on'],
                    'updated_on'          => $item['updated_on'],
                );
            }
        }

        return $files;
    }


    public function getUploadedAttachments()
    {

        $orderAttachments = Mage::getSingleton('redpandaplus_orderattachments/session')->getOrderAttachments();
        $files = array();

        if (!empty($orderAttachments)) {
            foreach ($orderAttachments as $item) {
                if (empty($item['hash'])) continue;
                $url = Mage::getUrl('orderattachments/index/getFile', array(
                    '_secure'=>true,
                    'file_id' => $item['hash']
                ));
                $files[] = array(
                    'id'                  => $item['order_attachment_id'],
                    'order_attachment_id' => $item['order_attachment_id'],
                    'product'             => $item['related_product_id'],
                    'url'                 => $url,
                    'file'                => $item['file'],
                    'file_name'           =>  basename($item['file']),
                    'comment'             => $this->htmlEscape($item['comment']),
                );
            }
        }

        return $files;
    }

    /**
     * Get Notification attachments
     *
     * @param RedPandaPlus_OrderAttachments_Model_Orderattachment[] $orderAttachments
     * @return array
     */
    public function getNotificationAttachments($orderAttachments)
    {
        $files = array();

        foreach ($orderAttachments as $orderAttachment) {
            $url = Mage::getUrl('orderattachments/index/getFile', array(
                '_secure'=>true,
                'file_id' => $orderAttachment->getData('hash')
            ));
            $files[] = array(
                'id'                  => $orderAttachment->getOrderAttachmentId(),
                'order_attachment_id' => $orderAttachment->getOrderAttachmentId(),
                'order_id'            => $orderAttachment->getOrderId(),
                'product'             => $orderAttachment->getRelatedProductId(),
                'url'                 => $url,
                'file'                => $orderAttachment->getFile(),
                'file_name'           => basename($orderAttachment->getFile()),
                'comment'             => $this->htmlEscape($orderAttachment->getComment()),
                'show'                => $orderAttachment->getVisibleCustomerAccount(),
                'created_on'          => $orderAttachment->getCreatedOn(),
                'updated_on'          => $orderAttachment->getUpdatedOn(),
            );
        }

        return $files;
    }

}
