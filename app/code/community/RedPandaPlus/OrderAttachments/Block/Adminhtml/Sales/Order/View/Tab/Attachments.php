<?php
class RedPandaPlus_OrderAttachments_Block_Adminhtml_Sales_Order_View_Tab_Attachments
    extends Mage_Adminhtml_Block_Template implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _construct()
    {
        parent::_construct();

        if (!$this->getTemplate()) {
            $this->setTemplate('redpandaplus_orderattachments/sales/order/view/tab/attachments.phtml');
            $this->setHtmlId('order_attachments');
        }
    }

    protected function _toHtml(){
    	if (!$this->getTemplate()) {
            return '';
        }
        $html = $this->renderView();
        return $html;
    }

    /**
     * Retrieve order model instance
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return Mage::registry('current_order');
    }


    public function getFieldId()
    {
        return 'order-attachment';
    }

    public function getFieldName()
    {
        return 'order_attachment';
    }

    public function getOrderAttachments()
    {

        $orderAttachments = Mage::getModel('redpandaplus_orderattachments/orderattachment')
                                ->getOrderAttachments($this->getOrder()->getId());

        $files = array();

        if (!empty($orderAttachments['items'])) {
            foreach ($orderAttachments['items'] as $item) {
                $files[] = array(
                    'id' => $item['order_attachment_id'],
                    'order_attachment_id' => $item['order_attachment_id'],
                    'order_id' => $item['order_id'],
                    'product' => $item['related_product_id'],
                    'url' => Mage::getUrl('orderattachments/index/getFile',
                                           array('_secure'=>true, 'file_id' => $item['hash'])),
                    'file' => $item['file'],
                    'file_name' =>  basename($item['file']),
                    'comment' => $this->htmlEscape($item['comment']),
                    'show' => $item['visible_customer_account'],
                    'created_on' => $item['created_on'],
                    'updated_on' => $item['updated_on'],
                );
            }
        }

        return $files;
    }

    public function getSubmitUrl()
    {
        $url = $this->getUrl('*/orderattachments/saveAttachments',
                             array('order_id'=>$this->getOrder()->getId()));

        return $url;
    }



    /**
     * ######################## TAB settings #################################
     */
    public function getTabLabel()
    {
        return Mage::helper('sales')->__('File Attachments');
    }

    public function getTabTitle()
    {
        return Mage::helper('sales')->__('File Attachments');
    }

    public function getTabClass()
    {
        return 'ajax only';
    }

    public function getClass()
    {
        return $this->getTabClass();
    }

    public function getTabUrl()
    {
        return $this->getUrl('adminhtml/orderattachments/fileAttachments', array('_current' => true));
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }


    protected function _prepareLayout()
    {

        $button = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
                'label'   => Mage::helper('redpandaplus_orderattachments')->__('Save Attachments Section'),
                'class'   => 'save',
                'id' => 'save-attachments-btn',
            ));
        $this->setChild('submit_button', $button);


        return parent::_prepareLayout();
    }
    
    public function getPermissionsForm()
    {
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getData('action'),
            'method' => 'post'
        ));

        $fieldset = $form->addFieldset('base_fieldset',
            array('legend'=>Mage::helper('redpandaplus_orderattachments')->__('Customer - Permissions Settings for Current Order'))
        );
        
        $fieldset->addField('can_delete_attachments', 'checkbox', array(
            'name'  => 'order_attachment_permissions[can_delete_attachments]',
            'label' => Mage::helper('redpandaplus_orderattachments')->__('Can Delete Attachments'),
            'title' => Mage::helper('redpandaplus_orderattachments')->__('Can Delete Attachments'),
            'class' => '',
            'required' => false,
            'value'   => '1',
        ));
        
        $fieldset->addField('can_edit_attachments', 'checkbox', array(
            'name'  => 'order_attachment_permissions[can_edit_attachments]',
            'label' => Mage::helper('redpandaplus_orderattachments')->__('Can Edit Attachments'),
            'title' => Mage::helper('redpandaplus_orderattachments')->__('Can Edit Attachments'),
            'class' => '',
            'required' => false,
            'value'   => '1',
        ));
        
        $fieldset->addField('can_add_new_attachments', 'checkbox', array(
            'name'  => 'order_attachment_permissions[can_add_new_attachments]',
            'label' => Mage::helper('redpandaplus_orderattachments')->__('Can Add New Attachments'),
            'title' => Mage::helper('redpandaplus_orderattachments')->__('Can Add New Attachments'),
            'class' => '',
            'required' => false,
            'value'   => '1',
        ));
        
        
                
        $form->getElement('can_delete_attachments')->setIsChecked(Mage::helper('redpandaplus_orderattachments')->getOrderPermissionStatus($this->getOrder(), 'can_delete_attachments'));
        $form->getElement('can_edit_attachments')->setIsChecked(Mage::helper('redpandaplus_orderattachments')->getOrderPermissionStatus($this->getOrder(), 'can_edit_attachments'));
        $form->getElement('can_add_new_attachments')->setIsChecked(Mage::helper('redpandaplus_orderattachments')->getOrderPermissionStatus($this->getOrder(), 'can_add_new_attachments'));
        
        return $form;
    }


}
