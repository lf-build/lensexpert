<?php
class RedPandaPlus_OrderAttachments_Block_Customer_Sales_Order_View_Attachments
    extends Mage_Core_Block_Template
{

    protected $_systemConfig;

    protected function _construct()
    {
        parent::_construct();

        $this->_systemConfig = Mage::helper('redpandaplus_orderattachments')->getConfiguration();

        if (!$this->getTemplate()) {
            $this->setTemplate('redpandaplus_orderattachments/sales/order/view/attachments.phtml');
            $this->setHtmlId('order_attachments');
        }
    }

    public function getSaveUrl()
    {
        return Mage::getUrl('orderattachments/index/saveAttachments',
                            array('order_id' => $this->getOrder()->getId()));
    }

    /**
     * Retrieve order model instance
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return Mage::registry('current_order');
    }


    public function getFieldId()
    {
        return 'order-attachment';
    }

    public function getFieldName()
    {
        return 'order_attachment';
    }


    public function getVisibleOrderAttachments()
    {

        $orderAttachments = Mage::getModel('redpandaplus_orderattachments/orderattachment')
                                ->getVisibleOrderAttachments($this->getOrder()->getId());

        $files = array();

        if (!empty($orderAttachments['items'])) {
            foreach ($orderAttachments['items'] as $item) {
                $url   = Mage::getUrl('orderattachments/index/getFile', array(
                    '_secure'=>true,
                    'file_id' => $item['hash']
                ));
                $files[] = array(
                    'id'                  => $item['order_attachment_id'],
                    'order_attachment_id' => $item['order_attachment_id'],
                    'order_id'            => $item['order_id'],
                    'product'             => $item['related_product_id'],
                    'url'                 => $url,
                    'file'                => $item['file'],
                    'file_name'           =>  basename($item['file']),
                    'comment'             => $this->htmlEscape($item['comment']),
                    'show'                => $item['visible_customer_account'],
                    'created_on'          => $item['created_on'],
                    'updated_on'          => $item['updated_on'],
                );
            }
        }

        return $files;
    }

    public function showAttachmentBlock()
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        return Mage::helper('redpandaplus_orderattachments')->customerCanUploadAttachment($customer);

        return false;
    }

    public function getManageAttachmentsUrl()
    {
        return Mage::getUrl('orderattachments/index/manageattachments',
                            array('order_id' => $this->getOrder()->getId()));
    }

    public function getOrderViewBackUrl()
    {
        return Mage::getUrl('sales/order/view',
                            array('order_id' => $this->getOrder()->getId()));
    }



    public function getSubmitUrl()
    {
        return $this->getUrl('*/orderattachments/saveAttachments', array('order_id'=>$this->getOrder()->getId()));
    }
}
