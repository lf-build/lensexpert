<?php
class RedPandaPlus_OrderAttachments_Block_Customer_List extends Mage_Core_Block_Template
{

    public function __construct()
    {
        parent::__construct();

        $customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
        $productId  = null;

        $orderAttachmentsCollection = Mage::getModel('redpandaplus_orderattachments/orderattachment')->getCollection()
            ->addFieldToFilter('visible_customer_account', 1)
            ->addFieldToFilter('customer_id', $customerId)
            ->joinOrders()
            ->setOrder('main_table.created_on', 'DESC');

        $this->setOrderAttachments($orderAttachmentsCollection);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $pager = $this->getLayout()->createBlock('page/html_pager', 'orderattachments.customer.list.pager')
            ->setCollection($this->getOrderAttachments());
        $this->setChild('pager', $pager);
        $this->getOrderAttachments()->load();
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }
}
