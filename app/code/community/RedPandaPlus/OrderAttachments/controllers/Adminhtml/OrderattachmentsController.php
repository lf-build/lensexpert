<?php
class RedPandaPlus_OrderAttachments_Adminhtml_OrderattachmentsController extends Mage_Adminhtml_Controller_Action
{


     /**
     * Initialize order model instance
     *
     * @return Mage_Sales_Model_Order || false
     */
    protected function _initOrder()
    {
        $id = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($id);

        if (!$order->getId()) {
            $this->_getSession()->addError($this->__('This order no longer exists.'));
            $this->_redirect('adminhtml/*/*');
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return false;
        }
        Mage::register('sales_order', $order);
        Mage::register('current_order', $order);
        return $order;
    }



    public function fileAttachmentsAction()
    {
        $this->_initOrder();
        $this->_initLayoutMessages('redpandaplus_orderattachments/session');
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('redpandaplus_orderattachments/adminhtml_sales_order_view_tab_attachments')->toHtml()
        );
    }

    public function saveAttachmentsAction()
    {

        if (!$order = $this->_initOrder()) {
            $this->_forward('noRoute');
            return false;
        }

        try{

            // the uploaded file was too big
            if (empty($_FILES)) {
                $msg = Mage::helper('redpandaplus_orderattachments')
                                ->__('Your file is too big, please upload a file less than %s', ini_get('upload_max_filesize'));
                Mage::throwException($msg);
            }

            $orderId = $this->getRequest()->getParam('order_id');
            
            // save order attachment permissions
            $defaultPermissions = array(
                'can_delete_attachments' => 0,
                'can_edit_attachments'   => 0,
                'can_add_new_attachments' => 0,
            );
            // we use a checkbox and must be sure all available values are properly set 
            $permissions = $this->getRequest()->getPost('order_attachment_permissions');
            foreach ($permissions as $k => $value) {
                $permissions[$k] = 1;
            }
            $permissions = json_encode(array_merge($defaultPermissions, $permissions));
            $order->setData('order_attachment_permissions', $permissions);
            $order->save();

            $data = array();
            $i    = 0;
            $orderAttachmentsCollection = array();
            foreach ($_POST as $id => $postData) {

                $orderAttachmentId = substr($id, strlen('order_attachment_'));

                // this is not an attachment record
                if (empty($orderAttachmentId)) continue;


                // -------------- upload files
                $newFileName = '';

                // new record - no attachment- ignore the record
                if (!$this->isValidAttachmentId($orderId, $orderAttachmentId) && empty($_FILES[$id]['tmp_name']['file'])) {
                   continue;
                } elseif (!empty($_FILES[$id]['tmp_name']['file'])) {
                    $newFileName = $this->uploadAttachment($id);
                }

                // ---------------- set the model data and save

                $orderAttachmentModel = Mage::getModel('redpandaplus_orderattachments/orderattachment');
                if ($this->isValidAttachmentId($orderId, $orderAttachmentId)) {
                    $orderAttachmentModel->load($orderAttachmentId);
                }
                if (!empty($postData['is_delete']) && $postData['is_delete'] == 1) {
                    $orderAttachmentModel->delete();
                    continue;
                }

                $orderAttachmentModel->setData('order_id', $orderId);
                if (isset($postData['comment'])) {
                    $orderAttachmentModel->setData('comment', $postData['comment']);
                }
                $orderAttachmentModel->setData('visible_customer_account', empty($postData['show'])? 0 : 1);
                $orderAttachmentModel->setData('related_product_id', '');
                if (!empty($newFileName)) {
                    $orderAttachmentModel->setData('old_file', $orderAttachmentModel->getData('file'));
                    $orderAttachmentModel->setData('file', $newFileName);
                    $orderAttachmentsCollection[] = $orderAttachmentModel;
                }
                $orderAttachmentModel->save();

                $i++;
            }
            
            Mage::dispatchEvent('order_attachments_updated', array(
                'order'             => $order,
                'order_attachments' => $orderAttachmentsCollection,
                'updated_by'        => 'admin',
            ));

            $msg = Mage::helper('redpandaplus_orderattachments')->__('%s attachments have been saved', $i);
            $this->getSession()->addSuccess($msg);

        } catch (Exception $e) {
            $msg = Mage::helper('redpandaplus_orderattachments')->__('An error occured: %s', $e->getMessage());
            $this->getSession()->addError($msg);
        }

        //$this->_redirect('adminhtml/orderattachments/fileAttachmentsFull', array('order_id'=>$orderId));
        return;

    }


    /**
     * Checks if an id is from an existing record
     *
     * @param int $orderId
     * @param string $id
     * @return RedPandaPlus_OrderAttachments_Model_Orderattachment|bool
     */
    private function isValidAttachmentId($orderId, $id) {
        $orderAttachment = Mage::getModel('redpandaplus_orderattachments/orderattachment')
                                ->getOrderAttachmentById($orderId, $id);


        if ($orderAttachment) return $orderAttachment;
        return false;
    }


    /**
     * Upload a file and returs a string with the filename (+relative location)
     *
     * @throws Mage_Core_Exception
     * @param int $id
     * @return string
     */
    private function uploadAttachment($id)
    {
        $newFileName = '';

        $uploader = new Varien_File_Uploader($id.'[file]');
        if ($allowedAttachments = Mage::helper('redpandaplus_orderattachments')->getAllowedExtensions()) {
            $uploader->setAllowedExtensions($allowedAttachments);
        }
        $uploader->addValidateCallback('redpandaplus_orderattachments_validation',
                                       Mage::helper('redpandaplus_orderattachments'),
                                       'validateUploadFile');
        $uploader->setAllowRenameFiles(true);
        $uploader->setFilesDispersion(true);
        $result = $uploader->save(
            Mage::helper('redpandaplus_orderattachments')->getFileSaveDirPath()
        );

        $newFileName = $result['file'];

        return $newFileName;
    }



    private function getSession()
    {
        return Mage::getSingleton('redpandaplus_orderattachments/session');
    }

}