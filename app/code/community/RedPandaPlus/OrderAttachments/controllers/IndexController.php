<?php
class RedPandaPlus_OrderAttachments_IndexController extends Mage_Core_Controller_Front_Action
{

   //orderattachments/index/index
   public function indexAction()
   {
   		$this->loadLayout();
        $this->_initLayoutMessages('redpandaplus_orderattachments/session');

        $this->renderLayout();
   }

    /**
     * Customer order attachments
     */
    public function listAction()
    {
        $action = $this->getRequest()->getActionName();
        $loginUrl = Mage::helper('customer')->getLoginUrl();

        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
            return;
        }

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');

        $this->getLayout()->getBlock('head')->setTitle($this->__('My Order Attachments'));

        if ($block = $this->getLayout()->getBlock('customer.account.link.back')) {
            $block->setRefererUrl($this->_getRefererUrl());
        }
        $this->renderLayout();
    }



   /**
     * Generate order attachments for ajax request
     */
    public function getCheckoutAttachmentsAction()
    {
        $this->_initLayoutMessages('redpandaplus_orderattachments/session');
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('redpandaplus_orderattachments/attachments')
                ->setTemplate('redpandaplus_orderattachments/checkout/onepage/attachments_block.phtml')
                ->toHtml()
        );
    }


    public function saveCheckoutAttachmentsAction()
    {
        if (!Mage::helper('redpandaplus_orderattachments')->getCanAddAttachments()) return;

        $customer = Mage::getSingleton('customer/session')->getCustomer();
        if (!Mage::helper('redpandaplus_orderattachments')->customerCanUploadAttachment($customer)) return;

        try{
            $orderId = $this->getRequest()->getParam('order_id');

            $orderAttachments = $this->getSession()->getOrderAttachments();


            // the uploaded file was too big
            if (isset($_SERVER['CONTENT_LENGTH']) && (int) $_SERVER['CONTENT_LENGTH'] > Mage::helper('redpandaplus_orderattachments')->toByteSize(ini_get('post_max_size'))) {
                $msg = Mage::helper('redpandaplus_orderattachments')
                                ->__('Your file is too big, please upload a file smaller than %s', ini_get('upload_max_filesize'));
                Mage::throwException($msg);
            }

            $data = array();
            $i = 0;
            foreach ($_POST as $id => $postData) {

                $orderAttachmentId = substr($id, strlen('order_attachment_'));

                // this is not an attachment record
                if (empty($orderAttachmentId)) continue;


                $data[$orderAttachmentId] = array(
                    'order_attachment_id' => $orderAttachmentId,
                    'comment' => '',
                    'visible_customer_account' => '',
                    'related_product_id' => '',
                );

                $oldOrderAttachment = '';
                if (!empty($orderAttachments[$orderAttachmentId])) {
                    $data[$orderAttachmentId] = $orderAttachments[$orderAttachmentId];
                    $oldOrderAttachment = $orderAttachments[$orderAttachmentId];
                }

                $data[$orderAttachmentId]['comment'] = $postData['comment'];

                if (!empty($postData['is_delete']) &&  $oldOrderAttachment) {
                    unset($data[$orderAttachmentId]);
                    $filePath = Mage::helper('redpandaplus_orderattachments')->getFilePath($oldOrderAttachment['file']);
                    @unlink($filePath);
                    continue;
                }

                // -------------- upload files
                $newFileName = '';

                // new record - no attachment- ignore the record
                if (!$oldOrderAttachment && empty($_FILES[$id]['tmp_name']['file'])) {
                   continue;
                } elseif (!empty($_FILES[$id]['tmp_name']['file'])) {
                    $newFileName = $this->uploadAttachment($id);
                }

                $numAllowedAttachments = Mage::helper('redpandaplus_orderattachments')->getNumberOfAllowedAttachments();
                if ($numAllowedAttachments !== '' && $i > $numAllowedAttachments) {
                    $msg = Mage::helper('redpandaplus_orderattachments')
                                ->__('Your are not allowed to have more than %s attachment(s) per order', $numAllowedAttachments);
                    Mage::throwException($msg);
                }

                if (!empty($newFileName)) {
                    $data[$orderAttachmentId]['file'] = $newFileName;
                    $hash = RedPandaPlus_OrderAttachments_Model_Orderattachment::getHash($newFileName);
                    $data[$orderAttachmentId]['hash'] = $hash;

                    // remove previous file
                    if (!empty($oldOrderAttachment)) {
                        $filePath = Mage::helper('redpandaplus_orderattachments')->getFilePath($oldOrderAttachment['file']);
                        @unlink($filePath);
                    }
                }

                $i++;
            }

            $this->getSession()->setOrderAttachments($data);

            $msg = Mage::helper('redpandaplus_orderattachments')->__('The attachments have been saved');
            $this->getSession()->addSuccess($msg);

        } catch (Exception $e) {
            $msg = Mage::helper('redpandaplus_orderattachments')->__('An error occured: %s', $e->getMessage());
            $this->getSession()->addError($msg);
        }

        return;
    }   
    

    /**
     * Generate order attachments for ajax request
     */
    public function fileAttachmentsAction()
    {
        $this->_initLayoutMessages('redpandaplus_orderattachments/session');
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('redpandaplus_orderattachments/attachments')->toHtml()
        );
    }

   public function manageAttachmentsAction()
   {
        if (!$this->_loadValidOrder()) {
            $this->_forward('noRoute');
            return false;
        }


        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('redpandaplus_orderattachments/session');

        if ($navigationBlock = $this->getLayout()->getBlock('customer_account_navigation')) {
            $navigationBlock->setActive('sales/order/history');
        }

        $this->renderLayout();
   }

   //orderattachments/index/getFile
   public function getFileAction()
    {

    	$fileId = $this->getRequest()->getParam('file_id');

    	if(!$fileId){
    		$this->_forward('noRoute');
            return false;
    	}

        $fileName = $this->getFileFromHash($fileId);

    	if(!$fileName) {
    		$this->_forward('noRoute');
            return false;
    	}

        $filePath = Mage::helper('redpandaplus_orderattachments')->getFilePath($fileName);


    	$this->showFile($filePath);
    }


    private function getFileFromHash($fileId)
    {

        $orderAttachments = $this->getSession()->getOrderAttachments();

        if (!empty($orderAttachments) && is_array($orderAttachments)) {
            foreach ($orderAttachments as $orderAttachment) {
                if (!empty($orderAttachment['hash']) && $orderAttachment['hash'] == $fileId) return $orderAttachment['file'];
            }
        }

        return Mage::getModel('redpandaplus_orderattachments/orderattachment')->getFileFromHash($fileId);
    }


    private function showFile($filePath)
    {
    	$file_path_parts = pathinfo($filePath);
    	$content = file_get_contents($filePath);

    	$contentType = Mage::helper('redpandaplus_orderattachments')->getContentTypeFromExtension($file_path_parts['extension']);

    	$this->prepareViewResponse($file_path_parts['basename'], $content, $contentType, $contentLength = null);
    }



   /**
     * Declare headers and content file in responce for file download
     *
     * @param string $fileName
     * @param string $content set to null to avoid starting output, $contentLength should be set explicitly in that case
     * @param string $contentType
     * @param int $contentLength explicit content length, if strlen($content) isn't applicable
     * @return RedPandaPlus_OrderAttachments_IndexController
     */
    private function prepareViewResponse($fileName, $content, $contentType = 'application/octet-stream', $contentLength = null)
    {

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', $contentType, true)
            ->setHeader('Content-Length', is_null($contentLength) ? strlen($content) : $contentLength)
            ->setHeader('Content-Disposition', 'inline; filename=' . $fileName)
            ->setHeader('Last-Modified', date('r'));
        if (!is_null($content)) {
            $this->getResponse()->setBody($content);
        }
        return $this;
    }


    /**
     * Action predispatch
     *
     * Check customer authentication for some actions
     */
    public function preDispatch()
    {
        parent::preDispatch();
        $action = $this->getRequest()->getActionName();

        $checkForActions = array('manageattachments');

        if (!in_array($action, $checkForActions)) {
            return;
        }

        $loginUrl = Mage::helper('customer')->getLoginUrl();

        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }


    /**
     * Check order view availability
     *
     * @param   Mage_Sales_Model_Order $order
     * @return  bool
     */
    protected function _canViewOrder($order)
    {
        $customerId = Mage::getSingleton('customer/session')->getCustomerId();
        $availableStates = Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates();
        if ($order->getId() && $order->getCustomerId() && ($order->getCustomerId() == $customerId)
            && in_array($order->getState(), $availableStates, $strict = true)
            ) {
            return true;
        }
        return false;
    }


    /**
     * Try to load valid order by order_id and register it
     *
     * @param int $orderId
     * @return bool
     */
    protected function _loadValidOrder($orderId = null)
    {
        if (null === $orderId) {
            $orderId = (int) $this->getRequest()->getParam('order_id');
        }
        if (!$orderId) {
            $this->_forward('noRoute');
            return false;
        }

        $order = Mage::getModel('sales/order')->load($orderId);

        if ($this->_canViewOrder($order)) {
            Mage::register('current_order', $order);
            return true;
        }
        else {
            $this->_redirect('sales/order/history');
        }
        return false;
    }

    public function saveAttachmentsAction()
    {
        if (!$this->_loadValidOrder()) {
            $this->_forward('noRoute');
            return false;
        }
        
        $order = Mage::registry('current_order');

        $canSaveAttachments = Mage::helper('redpandaplus_orderattachments')->getCanSaveAttachmentsForOrder($order);
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $customerCanUploadAttachment = Mage::helper('redpandaplus_orderattachments')->customerCanUploadAttachment($customer);

        $orderAttachmentsCollection = array();

        try{
            if (!$canSaveAttachments || !$customerCanUploadAttachment) {
                $msg = Mage::helper('redpandaplus_orderattachments')->__('Your are not allowed to save attachments');
                Mage::throwException($msg);
            }
            
            $orderId = $this->getRequest()->getParam('order_id');

            // the uploaded file was too big
            if (isset($_SERVER['CONTENT_LENGTH']) && (int) $_SERVER['CONTENT_LENGTH'] > Mage::helper('redpandaplus_orderattachments')->toByteSize(ini_get('post_max_size'))) {
                $msg = Mage::helper('redpandaplus_orderattachments')
                                ->__('Your file is too big, please upload a file smaller than %s', ini_get('upload_max_filesize'));
                Mage::throwException($msg);
            }

            $data = array();
            $i = 0;
            foreach ($_POST as $id => $postData) {

                $orderAttachmentId = substr($id, strlen('order_attachment_'));

                // this is not an attachment record
                if (empty($orderAttachmentId)) continue;

                $numAllowedAttachments = Mage::helper('redpandaplus_orderattachments')->getNumberOfAllowedAttachments();
                if ($numAllowedAttachments !== '' && $i > $numAllowedAttachments) {
                    $msg = Mage::helper('redpandaplus_orderattachments')
                                ->__('Your are not allowed to have more than %s attachment(s) per order', $numAllowedAttachments);
                    Mage::throwException($msg);
                }


                // ----------- check some permissions

                if (isset($postData['is_delete']) &&  $postData['is_delete'] == 1 &&
                        !Mage::helper('redpandaplus_orderattachments')->getOrderPermissionStatus($order, 'can_delete_attachments')) {
                    $msg = Mage::helper('redpandaplus_orderattachments')->__('Your are not allowed to delete attachments');
                    Mage::throwException($msg);
                }

                if (isset($postData['comment']) && $this->isValidAttachmentId($orderId, $orderAttachmentId)
                    && !Mage::helper('redpandaplus_orderattachments')->getOrderPermissionStatus($order, 'can_edit_attachments')) {
                    $msg = Mage::helper('redpandaplus_orderattachments')->__('Your are not allowed to edit attachments');
                    Mage::throwException($msg);
                }

                if (!empty($_FILES[$id]['tmp_name']['file'])) {
                    if (!$this->isValidAttachmentId($orderId, $orderAttachmentId)
                            && !Mage::helper('redpandaplus_orderattachments')->getOrderPermissionStatus($order, 'can_add_new_attachments')) {
                        $msg = Mage::helper('redpandaplus_orderattachments')->__('Your are not allowed to add new attachments');
                        Mage::throwException($msg);
                    }
                }

                // -------------- upload files
                $newFileName = '';

                // new record - ignore if no attachment
                if (!$this->isValidAttachmentId($orderId, $orderAttachmentId) && empty($_FILES[$id]['tmp_name']['file'])) {
                    continue;
                } elseif (!empty($_FILES[$id]['tmp_name']['file'])) {
                    $newFileName = $this->uploadAttachment($id);
                }

                // ---------------- set the model data and save

                $orderAttachmentModel = Mage::getModel('redpandaplus_orderattachments/orderattachment');
                if ($this->isValidAttachmentId($orderId, $orderAttachmentId)) {
                    $orderAttachmentModel->load($orderAttachmentId);
                }
                if (!empty($postData['is_delete']) && $postData['is_delete'] == 1) {
                    $orderAttachmentModel->delete();
                    continue;
                }

                $orderAttachmentModel->setData('order_id', $orderId);
                if (isset($postData['comment'])) {
                    $orderAttachmentModel->setData('comment', $postData['comment']);
                }
                $orderAttachmentModel->setData('visible_customer_account', 1);
                $orderAttachmentModel->setData('related_product_id', '');
                if (!empty($newFileName)) {
                    $orderAttachmentModel->setData('old_file', $orderAttachmentModel->getData('file'));
                    $orderAttachmentModel->setData('file', $newFileName);
                }
                $orderAttachmentModel->save();
                $orderAttachmentsCollection[] = $orderAttachmentModel;


                $i++;
            }

            $msg = Mage::helper('redpandaplus_orderattachments')->__('The attachments have been saved');
            $this->getSession()->addSuccess($msg);

        } catch (Exception $e) {
            $msg = Mage::helper('redpandaplus_orderattachments')->__('An error occured: %s', $e->getMessage());
            $this->getSession()->addError($msg);
        }

        Mage::dispatchEvent('order_attachments_updated', array(
            'order'             => Mage::getModel('sales/order')->load($orderId),
            'order_attachments' => $orderAttachmentsCollection,
            'updated_by'        => 'customer',
        ));
        
        // display block
        $this->_initLayoutMessages('redpandaplus_orderattachments/session');
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('redpandaplus_orderattachments/customer_sales_order_view_attachments')
                ->setTemplate('redpandaplus_orderattachments/sales/order/view/manage_attachments_block.phtml')
                ->toHtml()
        );

        //$this->_redirect('orderattachments/index/manageattachments', array('order_id' => $orderId));
        return;

    }

    /**
     * Upload a file and returs a string with the filename (+relative location)
     *
     * @throws Mage_Core_Exception
     * @param int $id
     * @return string
     */
    private function uploadAttachment($id)
    {
        $newFileName = '';

        $uploader = new Varien_File_Uploader($id.'[file]');

        if ($allowedAttachments = Mage::helper('redpandaplus_orderattachments')->getAllowedExtensions()) {
            $uploader->setAllowedExtensions($allowedAttachments);
        }
        $uploader->addValidateCallback('redpandaplus_orderattachments_validation',
                                       Mage::helper('redpandaplus_orderattachments'),
                                       'validateUploadFile');
        $uploader->setAllowRenameFiles(true);
        $uploader->setFilesDispersion(true);
        $result = $uploader->save(
            Mage::helper('redpandaplus_orderattachments')->getFileSaveDirPath()
        );

        $newFileName = $result['file'];

        return $newFileName;
    }


    /**
     * Checks if an id is from an existing record
     *
     * @param int $orderId
     * @param string $id
     * @return RedPandaPlus_OrderAttachments_Model_Orderattachment|bool
     */
    private function isValidAttachmentId($orderId, $id) {
        $orderAttachment = Mage::getModel('redpandaplus_orderattachments/orderattachment')
                                ->getOrderAttachmentById($orderId, $id);


        if ($orderAttachment) return $orderAttachment;
        return false;
    }


    private function getSession()
    {
        return Mage::getSingleton('redpandaplus_orderattachments/session');
    }



}
