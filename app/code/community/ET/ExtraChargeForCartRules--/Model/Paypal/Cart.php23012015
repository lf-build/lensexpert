<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_ExtraChargeForCartRules
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-commercial-v1/   ETWS Commercial License (ECL1)
 */


/**
 * PayPal-specific model for shopping cart items and totals
 * The main idea is to accommodate all possible totals into PayPal-compatible 4 totals and line items
 */
class ET_ExtraChargeForCartRules_Model_Paypal_Cart extends Mage_Paypal_Model_Cart
{

    /**
     * (re)Render all items and totals
     *
     * rewriting function because PayPal does not support discount >0
     */
    protected function _render()
    {
        if (!$this->_shouldRender) {
            return;
        }

        $storeId = $this->_salesEntity->getStoreId();

        if (Mage::helper('core')->isModuleEnabled('ET_PaymentExtraCharge')
            && Mage::helper('et_paymentextracharge')->isEnabledPaymentExtraCharge($storeId)
        ) {
            $paymentextrachargeHelper = Mage::helper('et_paymentextracharge');
        }

        if (Mage::helper('core')->isModuleEnabled('ET_ExtraChargeForCartRules')
            && Mage::helper('etextrachargeforcartrules')->isActive()
            && Mage::helper('etextrachargeforcartrules')->isPaypalDiscountFixEnabled()
        ) {
            $extrachargeforcartrulesHelper = Mage::helper('etextrachargeforcartrules');
        }

        if ((isset($paymentextrachargeHelper) && $paymentextrachargeHelper->isEnabledPaymentExtraCharge($storeId)) ||
            (
                isset($extrachargeforcartrulesHelper)
                && $extrachargeforcartrulesHelper->isActive()
                && $extrachargeforcartrulesHelper->isPaypalDiscountFixEnabled()
            )
        ) {
            // regular items from the sales entity
            $this->_items = array();
            foreach ($this->_salesEntity->getAllItems() as $item) {
                if (!$item->getParentItem()) {
                    $this->_addRegularItem($item);
                }
            }
            end($this->_items);
            $lastRegularItemKey = key($this->_items);

            // regular totals
            $shippingDescription = '';
            $extraCharge = 0;

            if ($this->_salesEntity instanceof Mage_Sales_Model_Order) {
                if (isset($paymentextrachargeHelper)
                    && $paymentextrachargeHelper->isEnabledPaymentExtraCharge($storeId)
                    && $this->_salesEntity->getData('et_payment_extra_charge_excluding_tax') > 0
                ) {
                    $extraCharge = $this->_salesEntity->getData('et_payment_extra_charge_excluding_tax');
                    $paymentExtraCharge = $extraCharge;
                }
                $discount = $this->_salesEntity->getBaseDiscountAmount();

                if ((isset($extrachargeforcartrulesHelper))
                ) {
                    if ($this->_salesEntity->getBaseDiscountAmount() > 0) {
                        $discount = 0;
                        $extraCharge = $extraCharge + abs($this->_salesEntity->getBaseDiscountAmount());
                        $extraChargeForCartRules = abs($this->_salesEntity->getBaseDiscountAmount());
                    }
                }

                $shippingDescription = $this->_salesEntity->getShippingDescription();
                $this->_totals = array(
                    self::TOTAL_SUBTOTAL => $this->_salesEntity->getBaseSubtotal(),
                    self::TOTAL_TAX => $this->_salesEntity->getBaseTaxAmount(),
                    self::TOTAL_SHIPPING => $this->_salesEntity->getBaseShippingAmount(),
                    self::TOTAL_DISCOUNT => abs($discount),
                );
                $this->_applyHiddenTaxWorkaround($this->_salesEntity);
            } else {
                $address = $this->_salesEntity->getIsVirtual() ?
                    $this->_salesEntity->getBillingAddress() : $this->_salesEntity->getShippingAddress();
                $shippingDescription = $address->getShippingDescription();
                $this->_totals = array(
                    self::TOTAL_SUBTOTAL => $this->_salesEntity->getBaseSubtotal(),
                    self::TOTAL_TAX => $address->getBaseTaxAmount(),
                    self::TOTAL_SHIPPING => $address->getBaseShippingAmount(),
                    self::TOTAL_DISCOUNT => abs($address->getBaseDiscountAmount()),
                );
                $this->_applyHiddenTaxWorkaround($address);
            }
            $originalDiscount = $this->_totals[self::TOTAL_DISCOUNT];

            // arbitrary items, total modifications
            Mage::dispatchEvent('paypal_prepare_line_items', array('paypal_cart' => $this));

            // distinguish original discount among the others
            if ($originalDiscount > 0.0001 && isset($this->_totalLineItemDescriptions[self::TOTAL_DISCOUNT])) {
                $this->_totalLineItemDescriptions[self::TOTAL_DISCOUNT][] = Mage::helper('sales')->__('Discount (%s)',
                    Mage::app()->getStore()->convertPrice($originalDiscount, true, false));
            }

            // discount, shipping as items
            if ($this->_isDiscountAsItem && $this->_totals[self::TOTAL_DISCOUNT]) {
                $this->addItem(Mage::helper('paypal')->__('Discount'), 1, -1.00 * $this->_totals[self::TOTAL_DISCOUNT],
                    $this->_renderTotalLineItemDescriptions(self::TOTAL_DISCOUNT)
                );
            }
            $shippingItemId = $this->_renderTotalLineItemDescriptions(self::TOTAL_SHIPPING, $shippingDescription);
            if ($this->_isShippingAsItem && (float)$this->_totals[self::TOTAL_SHIPPING]) {
                $this->addItem(Mage::helper('paypal')->__('Shipping'), 1, (float)$this->_totals[self::TOTAL_SHIPPING],
                    $shippingItemId
                );
            }

            if ($this->_isShippingAsItem && ($extraCharge > 0)) {
                if (isset($paymentextrachargeHelper)
                    && isset($paymentExtraCharge)
                    && $paymentExtraCharge > 0
                ) {
                    $this->addItem($paymentextrachargeHelper->getTotalTitle($storeId, 'paypal_standard'), 1, (float)$paymentExtraCharge,
                        $this->_renderTotalLineItemDescriptions('etpaymentextracharge')
                    );
                }
                if (isset($extrachargeforcartrulesHelper)
                    && isset($extraChargeForCartRules)
                    && $extraChargeForCartRules > 0
                ) {
                    $this->addItem($extrachargeforcartrulesHelper->__('Extra charge'), 1,
                        (float)$extraChargeForCartRules,
                        $this->_renderTotalLineItemDescriptions('extracharge')
                    );
                }
            } else {
                $this->_totals[self::TOTAL_SUBTOTAL] += $extraCharge;
            }

            // compound non-regular items into subtotal
            foreach ($this->_items as $key => $item) {
                if ($key > $lastRegularItemKey && $item->getAmount() != 0) {
                    $this->_totals[self::TOTAL_SUBTOTAL] += $item->getAmount();
                }
            }
            $this->_validate();
            // if cart items are invalid, prepare cart for transfer without line items
            if (!$this->_areItemsValid) {
                $this->removeItem($shippingItemId);
            }

            $this->_shouldRender = false;
        } else {
            return parent::_render();
        }
    }

    /*Rewrite because is private and can not be used*/
    private function _applyHiddenTaxWorkaround($salesEntity)
    {
        $this->_totals[self::TOTAL_TAX] += (float)$salesEntity->getBaseHiddenTaxAmount();
        $this->_totals[self::TOTAL_TAX] += (float)$salesEntity->getBaseShippingHiddenTaxAmount();
    }
}
