<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_ExtraChargeForCartRules
 * @copyright  Copyright (c) 2012 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-commercial-v1/   ETWS Commercial License (ECL1)
 */

class ET_ExtraChargeForCartRules_Model_Rule extends Mage_SalesRule_Model_Rule
{
    /**
     * Prepare data before saving
     *
     * @return Mage_Rule_Model_Rule
     */
    protected function _beforeSave()
    {
        $moduleHelper = Mage::helper('etextrachargeforcartrules');
        if ($moduleHelper->isActive()) {
            $realDiscountAmount = $this->getDiscountAmount();

            // check if discount amount > 0
            if ((int)$this->getDiscountAmount() < 0) {
                $this->setDiscountAmount(0);
            }

            parent::_beforeSave();

            if ($realDiscountAmount < 0) {
                $this->setDiscountAmount($realDiscountAmount);
            }

            return $this;
        } else {
            return parent::_beforeSave();
        }

    }

}