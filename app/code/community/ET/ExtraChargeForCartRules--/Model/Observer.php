<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_ExtraChargeForCartRules
 * @copyright  Copyright (c) 2012 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-commercial-v1/   ETWS Commercial License (ECL1)
 */

class ET_ExtraChargeForCartRules_Model_Observer
{
    /**
     * Change Discount Label if need (Discount or Extra Charge)
     *
     * @param Varien_Event_Observer $observer
     */
    public function changeDiscountLabel(Varien_Event_Observer $observer)
    {
        /** @var $quote Mage_Sales_Model_Quote */
        $quote = $observer->getEvent()->getQuote();

        foreach ($quote->getAllAddresses() as $address) {
            /* @var $address Mage_Sales_Model_Quote_Address */
            //echo $address->getDiscountDescription() . "<br>";
            $totalsArray = $address->getTotals();
            /** @var $total Mage_Sales_Model_Quote_Address_Total */
            foreach ($totalsArray as $total) {
                echo $total->getCode() . " - " . $total->toJson() . "<br>";
                if ($total->getCode() == 'discount') {
                    if ($total->getValue() > 0) {
                        $total->setTitle('Extra charge');
                        //echo $total->toJson();
                    }
                }
                //echo $total->toJson();
                //echo get_class($total) . "<br>";
            }
        }

    }

    /**
     * Reprocess rule if its action type Mage_SalesRule_Model_Rule::CART_FIXED_ACTION
     * and extra charge
     *
     * Event: salesrule_validator_process
     *
     * @param Varien_Event_Observer $observer
     */
    public function reProcessCartRuleCartFixedAction(Varien_Event_Observer $observer)
    {
        /** @var $rule Mage_SalesRule_Model_Rule */
        $rule = $observer->getEvent()->getRule();

        //if ($rule->getSimpleAction() == Mage_SalesRule_Model_Rule::CART_FIXED_ACTION) {
        if ($rule->getSimpleAction() == 'cart_fixed') {
            /** @var $address Mage_Sales_Model_Quote_Address */
            $address = $observer->getEvent()->getAddress();
            $quote = $observer->getEvent()->getQuote();
            $qty = $observer->getEvent()->getQty();
            $result = $observer->getEvent()->getResult();

            //$itemPrice =
            //$baseItemPrice =
            //$itemOriginalPrice =
            //$baseItemOriginalPrice =


            $cartRules = $address->getCartFixedRules();
            if (!isset($cartRules[$rule->getId()])) {
                $cartRules[$rule->getId()] = $rule->getDiscountAmount();
            }

            //var_dump($cartRules);
            // only for extra charge
            if ($cartRules[$rule->getId()] < 0) {

                /*
                    if ($this->_rulesItemTotals[$rule->getId()]['items_count'] <= 1) {
                        $quoteAmount = $quote->getStore()->convertPrice($cartRules[$rule->getId()]);
                        $baseDiscountAmount = min($baseItemPrice * $qty, $cartRules[$rule->getId()]);
                    } else {
                        $discountRate = $baseItemPrice * $qty /
                            $this->_rulesItemTotals[$rule->getId()]['base_items_price'];
                        $maximumItemDiscount = $rule->getDiscountAmount() * $discountRate;
                        $quoteAmount = $quote->getStore()->convertPrice($maximumItemDiscount);

                        $baseDiscountAmount = min($baseItemPrice * $qty, $maximumItemDiscount);
                        $this->_rulesItemTotals[$rule->getId()]['items_count']--;
                    }

                    $discountAmount = min($itemPrice * $qty, $quoteAmount);
                    $discountAmount = $quote->getStore()->roundPrice($discountAmount);
                    $baseDiscountAmount = $quote->getStore()->roundPrice($baseDiscountAmount);

                    //get discount for original price
                    $originalDiscountAmount = min($itemOriginalPrice * $qty, $quoteAmount);
                    $baseOriginalDiscountAmount = $quote->getStore()->roundPrice($originalDiscountAmount);
                    $baseOriginalDiscountAmount = $quote->getStore()->roundPrice($baseItemOriginalPrice);

                    $cartRules[$rule->getId()] -= $baseDiscountAmount;
                */
                $address->setCartFixedRules($cartRules);

                $discountAmount = $cartRules[$rule->getId()] - 7;
                $baseDiscountAmount = $cartRules[$rule->getId()] - 7;

                //$result->setDiscountAmount($discountAmount);
                //$result->setBaseDiscountAmount($baseDiscountAmount);

            } // end only for extra charge
        } // end action = 'cart_fixed'
    }

    /**
     *
     * @param Varien_Event_Observer $observer
     */
    public function rewriteClasses(Varien_Event_Observer $observer)
    {
        $isRewriteEnabled = (int)Mage::getConfig()->getNode('default/etextrachargeforcartrules/general/rewrite_classes');
        if ($isRewriteEnabled) {
            /** in CE version 1.8.1.0 tax functions declarations changed */
            if (version_compare(Mage::getVersion(), '1.8.1', '>')) {
                Mage::getConfig()->setNode(
                    'global/models/tax/rewrite/sales_total_quote_tax',
                    'ET_ExtraChargeForCartRules_Model_Tax_Sales_Total_Quote_Tax1810'
                );
            } else {
                Mage::getConfig()->setNode(
                    'global/models/tax/rewrite/sales_total_quote_tax',
                    'ET_ExtraChargeForCartRules_Model_Tax_Sales_Total_Quote_Tax'
                );
            }
            /** in CE version 1.8.0.0 collect discount total function changed **/
            if (version_compare(Mage::getVersion(), '1.8.0', '>')) {
                Mage::getConfig()->setNode(
                    'global/models/sales/rewrite/order_invoice_total_discount',
                    'ET_ExtraChargeForCartRules_Model_Order_Invoice_Total_Discount'
                );
            }
        }
    }

}