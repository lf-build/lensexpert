<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_ExtraChargeForCartRules
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-commercial-v1/   ETWS Commercial License (ECL1)
 */

class ET_ExtraChargeForCartRules_Block_Adminhtml_Sales_Order_Invoice_Totals
    extends Mage_Adminhtml_Block_Sales_Order_Invoice_Totals
{
    /**
     * Initialize order totals array
     *
     * @return ET_ExtraChargeForCartRules_Block_Adminhtml_Sales_Order_Invoice_Totals
     */
    public function _initTotals()
    {

        /** @var $moduleHelper ET_ExtraChargeForCartRules_Helper_Data */
        $moduleHelper = Mage::helper('etextrachargeforcartrules');
        if ($moduleHelper->isActive()) {
            parent::_initTotals();

            foreach ($this->_totals as $total) {
                if ($total->getCode() == 'discount') {
                    if ($total->getValue() < 0) {
                        $description = $this->getOrder()->getDiscountDescription();
                        if ($description) {
                            $title = $moduleHelper->__('Extra charge (%s)', $description);
                        } else {
                            $title = $moduleHelper->__('Extra charge');
                        }
                        $this->_totals['discount']->setLabel($title);
                        $this->_totals['discount']->setValue($total->getValue() * -1);
                    }
                }
            }
            return $this;
        } else {
            return parent::_initTotals();
        }

    }

}