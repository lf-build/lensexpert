<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_ExtraChargeForCartRules
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-commercial-v1/   ETWS Commercial License (ECL1)
 */

class ET_ExtraChargeForCartRules_Model_Tax_Sales_Pdf_Discount extends Mage_Sales_Model_Order_Pdf_Total_Default
{
    /**
     * Get array of arrays with totals information for display in PDF
     * array(
     *  $index => array(
     *      'amount'   => $amount,
     *      'label'    => $label,
     *      'font_size'=> $font_size
     *  )
     * )
     * @return array
     */
    public function getTotalsForDisplay()
    {
        $store = $this->getOrder()->getStore();
        /** @var $moduleHelper ET_ExtraChargeForCartRules_Helper_Data */
        $moduleHelper = Mage::helper('etextrachargeforcartrules');
        $amount = $this->getAmount();
        if ($amount < 0) {
            $description = $this->getOrder()->getDiscountDescription();
            if ($description) {
                $title = $moduleHelper->__('Extra charge (%s)', $description);
            } else {
                $title = $moduleHelper->__('Extra charge');
            }

            $label = $title . ':';
            $amount *= -1;
        } else {
            $label = Mage::helper('sales')->__($this->getTitle()) . ':';
        }


        $amountFormatted = $this->getOrder()->formatPriceTxt($amount);
        $fontSize = $this->getFontSize() ? $this->getFontSize() : 7;

        $totals = array(array(
            'amount' => $amountFormatted,
            'label' => $label,
            'font_size' => $fontSize
        ));

        return $totals;
    }
}