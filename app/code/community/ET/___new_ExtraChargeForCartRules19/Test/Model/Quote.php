<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_ExtraChargeForCartRules
 * @copyright  Copyright (c) 2013 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-commercial-v1/   ETWS Commercial License (ECL1)
 */


class ET_ExtraChargeForCartRules_Test_Model_Quote extends EcomDev_PHPUnit_Test_Case
{

    /**
     * Тестирование - идея
     *
     * надо проверять суммы тоталов в квоте, заказе и для пэйпала
     * надо проверять лабел тотала (скидка или наценка) + (есть лабел или нет - что именно за скидка/наценка)
     *
     * Исходные данные Тест 1 (проверка расчёта конечной суммы):
     * 1. Модуль отключен
     * 2. Есть товар, стоимость 1000.00
     * 3. Налоги отключены
     * 4. Валюта базовая и отображаемая евро
     * 5. Доставка фиксированная 5 евро
     * 6. В квоте grandtotal должен быть 1005.00
     *
     * Исходные данные Тест 2 (проверка расчёта конечной суммы):
     * 1. Модуль отключен
     * 2. Есть товар, стоимость 1000.00
     * 3. Налоги отключены
     * 4. Валюта базовая и отображаемая евро
     * 5. Доставка фиксированная 5 евро
     * 6. есть правило корзины на скидку 11 процентов
     * 7. В квоте grandtotal должен быть 994.00
     *
     *
     * Исходные данные Тест 3 (проверка расчёта конечной суммы):
     * 1. Модуль отключен
     * 2. Есть товар, стоимость 1000.00
     * 3. Налоги отключены
     * 4. Валюта базовая и отображаемая евро
     * 5. Доставка фиксированная 5 евро
     * 6. есть правило корзины на наценку 11 процентов
     * 7. В квоте grandtotal должен быть 1016.00
     *
     *
     *
     */

    /**
     * Quote GrandTotal calculation test
     *
     * @test
     * @doNotIndexAll
     * //////loadFixture
     * @dataProvider dataProvider
     */
    public function checkQuoteGrandTotal($one, $two)
    {
        //$this->setCurrentStore('default');
        //$storeId = Mage::app()->getStore()->getId();
        //$storeId = Mage::app()->getStore(4)->getId();
        // store 0
        // if we need other - need create

        // expected value, value to check
        $this->assertEquals($one, $two);
        $this->assertEquals(10, $storeId);

    }


}