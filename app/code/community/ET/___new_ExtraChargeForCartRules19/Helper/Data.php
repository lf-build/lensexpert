<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_ExtraChargeForCartRules
 * @copyright  Copyright (c) 2012 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-commercial-v1/   ETWS Commercial License (ECL1)
 */

class ET_ExtraChargeForCartRules_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Check is module enabled by user or not
     * @return bool
     */
    public function isActive()
    {
        return Mage::getStoreConfig('etextrachargeforcartrules/general/enabled');
    }

    public function isPaypalDiscountFixEnabled()
    {
        return Mage::getStoreConfig('etextrachargeforcartrules/general/fix_paypal_discount');
    }

    public function isHiddenTaxFixEnabled()
    {
        // Actual for some Magento CE versions with tax setting
        // System -> Configuration -> Sales -> Tax -> Calculation Settings -> Apply Customer Tax = After Discount
        return Mage::getStoreConfig('etextrachargeforcartrules/general/fix_hidden_tax');
    }
}