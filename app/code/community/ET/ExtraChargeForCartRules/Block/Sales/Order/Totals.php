<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_ExtraChargeForCartRules
 * @copyright  Copyright (c) 2012 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-commercial-v1/   ETWS Commercial License (ECL1)
 */

class ET_ExtraChargeForCartRules_Block_Sales_Order_Totals extends Mage_Sales_Block_Order_Totals
{
    /**
     * Initialize order totals array
     *
     * @return Mage_Sales_Block_Order_Totals
     */
    protected function _initTotals()
    {
        /** @var $moduleHelper ET_ExtraChargeForCartRules_Helper_Data */
        $moduleHelper = Mage::helper('etextrachargeforcartrules');
        if ($moduleHelper->isActive()) {
            parent::_initTotals();

            foreach ($this->_totals as $total) {
                if ($total->getCode() == 'discount') {
//                    var_dump($total->getValue());
//                    exit('etws sales');
                    if ($total->getValue() > 0) {
                        $description = $this->getSource()->getDiscountDescription();
                        if ($description) {
                            $title = $moduleHelper->__('(%s)', $description);
                        } else {
                            $title = $moduleHelper->__('');
                        }
                        $this->_totals['discount']->setLabel($title);
                    }
                }
            }
            return $this;
        } else {
            return parent::_initTotals();
        }

    }

}