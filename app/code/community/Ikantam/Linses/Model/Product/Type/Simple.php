<?php

class Ikantam_Linses_Model_Product_Type_Simple extends Mage_Catalog_Model_Product_Type_Simple
{
    protected function _prepareProduct(Varien_Object $buyRequest, $product, $processMode) {
        /* Mage::log($product->getData());
          Mage::log('-------------------');
          Mage::log($buyRequest );
          die(); */

        $product = $this->getProduct($product);
        /* @var Mage_Catalog_Model_Product $product */
        // try to add custom options

        try {
            $options = $this->_prepareOptions($buyRequest, $product, $processMode);
        } catch (Mage_Core_Exception $e) {
            return $e->getMessage();
        }

        if (is_string($options)) {
            return $options;
        }
        // try to found super product configuration
        // (if product was buying within grouped product)
        $superProductConfig = $buyRequest->getSuperProductConfig();
        if (!empty($superProductConfig['product_id'])
                && !empty($superProductConfig['product_type'])
        ) {
            $superProductId = (int) $superProductConfig['product_id'];
            if ($superProductId) {
                if (!$superProduct = Mage::registry('used_super_product_' . $superProductId)) {
                    $superProduct = Mage::getModel('catalog/product')->load($superProductId);
                    Mage::register('used_super_product_' . $superProductId, $superProduct);
                }
                if ($superProduct->getId()) {
                    $assocProductIds = $superProduct->getTypeInstance(true)->getAssociatedProductIds($superProduct);
                    if (in_array($product->getId(), $assocProductIds)) {
                        $productType = $superProductConfig['product_type'];
                        $product->addCustomOption('product_type', $productType, $superProduct);

                        $buyRequest->setData('super_product_config', array(
                            'product_type' => $productType,
                            'product_id' => $superProduct->getId()
                        ));
                    }
                }
            }
        }
        //$dataCount = $buyRequest->getData('super_options');
        //mage::log($dataCount[$product->getId()]);
        /*
          $product->setCustomOptions($dataCount); */
        /* @var $currentModel Mage_Catalog_Model_Product */

        //$product->setCustomOptions();
        //$array = $dataCount[$product->getId()];
        /* $currentModel = Mage::getModel('catalog/product')->load($product->getId());
          $options = $currentModel->getOptions();
          foreach($options as $option){
          $option->setData('value', 1);
          mage::log($option->getData());

          }
          //  mage::log($option->getData());
          //if (!empty($option)){


          //$option->save();

          //}



          //}

          //Mage::log();
          //$product->getOptions()
          $product->setCustomOptions($options); */

        $product->prepareCustomOptions();
        $buyRequest->unsetData('_processing_params'); // One-time params only
        $product->addCustomOption('info_buyRequest', serialize($buyRequest->getData()));

        if ($options) {
            $optionIds = array_keys($options);
            $product->addCustomOption('option_ids', implode(',', $optionIds));
            foreach ($options as $optionId => $optionValue) {
                $product->addCustomOption(self::OPTION_PREFIX . $optionId, $optionValue);
            }
        }

        if ($superOptions = $buyRequest->getData('super_options')) {
            $superOptionCurrent = $superOptions[$product->getId()];


            $superOptionIds = array_keys($superOptionCurrent);
            $product->addCustomOption('option_ids', implode(',', $superOptionIds));
            foreach ($superOptionCurrent as $superOptionId => $superOptionValue) {
                $product->addCustomOption(self::OPTION_PREFIX . $superOptionId, $superOptionValue);
            }
        }

        // set quantity in cart
        if ($this->_isStrictProcessMode($processMode)) {
            $product->setCartQty($buyRequest->getQty());
        }
        $product->setQty($buyRequest->getQty());

        return array($product);
    }

    
        protected function _prepareOptionsForCart(Varien_Object $buyRequest, $product = null) {
        $newOptions = array();
        foreach ($this->getProduct($product)->getProductOptionsCollection() as $_option) {

            /* @var $_option Mage_Catalog_Model_Product_Option */
            $group = $_option->groupFactory($_option->getType())
                    ->setOption($_option)
                    ->setProduct($this->getProduct($product))
                    ->setRequest($buyRequest);

            $superOptions = $buyRequest->getSuperOptions();
            if ($superOptions && isset($superOptions[$product->getId()])) {
                $group->validateUserValue($superOptions[$product->getId()]);
            } else {
                $group->validateUserValue($buyRequest->getOptions());
            }

            $preparedValue = $group->prepareForCart();

            if ($preparedValue !== null) {
                $newOptions[$_option->getId()] = $preparedValue;
            }
        }
        return $newOptions;
    }
}
