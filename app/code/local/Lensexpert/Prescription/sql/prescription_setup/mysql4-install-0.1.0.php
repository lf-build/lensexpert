<?php
/**
 * Lensexpert
 *
 * @category    Lensexpert
 * @package     Lensexpert_Prescription
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

$installer = $this;
/* @var $installer Lensexpert_Prescription_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();
$installer->run("
     DROP TABLE IF EXISTS {$this->getTable('prescription/prescription')};
        CREATE TABLE {$this->getTable('prescription/prescription')} (
          `prescription_id` int(11) unsigned NOT NULL auto_increment,
		  `customer_id` int(11) unsigned NOT NULL,
		  `file` varchar(255),
		  `comment` text,
		  `created_on` datetime,
		  `updated_on` datetime,
		  PRIMARY KEY (`prescription_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		");
$installer->endSetup();