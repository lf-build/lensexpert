<?php 
/**
 * Lensexpert
 *
 * @category    Lensexpert
 * @package     Lensexpert_Prescription
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Lensexpert_Prescription_PrescriptionController extends Mage_Core_Controller_Front_Action {

    protected function _getSession() {
        return Mage::getSingleton('customer/session');
    }

    public function preDispatch() {
        parent::preDispatch();
            if (!Mage::getSingleton('customer/session')->authenticate($this)) {
                $this->setFlag('', 'no-dispatch', true);
        }
    }
	
    public function indexAction() { // landing page
        $this->loadLayout();
        $this->renderLayout();

    }
    
}