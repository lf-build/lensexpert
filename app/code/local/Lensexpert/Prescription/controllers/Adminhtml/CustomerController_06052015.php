<?php
/**
 * Lensexpert
 *
 * @category    Lensexpert
 * @package     Lensexpert_Prescription
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
include_once("Mage/Adminhtml/controllers/CustomerController.php");

class Lensexpert_Prescription_Adminhtml_CustomerController extends Mage_Adminhtml_CustomerController
{
    public function saveAction(){
        $data = $this->getRequest()->getPost();
        if ($data) {


            $redirectBack = $this->getRequest()->getParam('back', false);
            $this->_initCustomer('customer_id');

            /** @var $customer Mage_Customer_Model_Customer */
            $customer = Mage::registry('current_customer');

            /** @var $customerForm Mage_Customer_Model_Form */
            $customerForm = Mage::getModel('customer/form');
            $customerForm->setEntity($customer)
                    ->setFormCode('adminhtml_customer')
                    ->ignoreInvisible(false)
            ;

            $formData = $customerForm->extractData($this->getRequest(), 'account');

            // Handle 'disable auto_group_change' attribute
            if (isset($formData['disable_auto_group_change'])) {
                $formData['disable_auto_group_change'] = empty($formData['disable_auto_group_change']) ? '0' : '1';
            }

            $errors = $customerForm->validateData($formData);
            if ($errors !== true) {
                foreach ($errors as $error) {
                    $this->_getSession()->addError($error);
                }
                $this->_getSession()->setCustomerData($data);
                $this->getResponse()->setRedirect($this->getUrl('*/customer/edit', array('id' => $customer->getId())));
                return;
            }

            $customerForm->compactData($formData);

            // Unset template data
            if (isset($data['address']['_template_'])) {
                unset($data['address']['_template_']);
            }

            $modifiedAddresses = array();
            if (!empty($data['address'])) {
                /** @var $addressForm Mage_Customer_Model_Form */
                $addressForm = Mage::getModel('customer/form');
                $addressForm->setFormCode('adminhtml_customer_address')->ignoreInvisible(false);

                foreach (array_keys($data['address']) as $index) {
                    $address = $customer->getAddressItemById($index);
                    if (!$address) {
                        $address = Mage::getModel('customer/address');
                    }

                    $requestScope = sprintf('address/%s', $index);
                    $formData = $addressForm->setEntity($address)
                            ->extractData($this->getRequest(), $requestScope);

                    // Set default billing and shipping flags to address
                    $isDefaultBilling = isset($data['account']['default_billing']) && $data['account']['default_billing'] == $index;
                    $address->setIsDefaultBilling($isDefaultBilling);
                    $isDefaultShipping = isset($data['account']['default_shipping']) && $data['account']['default_shipping'] == $index;
                    $address->setIsDefaultShipping($isDefaultShipping);

                    $errors = $addressForm->validateData($formData);
                    if ($errors !== true) {
                        foreach ($errors as $error) {
                            $this->_getSession()->addError($error);
                        }
                        $this->_getSession()->setCustomerData($data);
                        $this->getResponse()->setRedirect($this->getUrl('*/customer/edit', array(
                                    'id' => $customer->getId())
                        ));
                        return;
                    }

                    $addressForm->compactData($formData);

                    // Set post_index for detect default billing and shipping addresses
                    $address->setPostIndex($index);

                    if ($address->getId()) {
                        $modifiedAddresses[] = $address->getId();
                    } else {
                        $customer->addAddress($address);
                    }
                }
            }

            // Default billing and shipping
            if (isset($data['account']['default_billing'])) {
                $customer->setData('default_billing', $data['account']['default_billing']);
            }
            if (isset($data['account']['default_shipping'])) {
                $customer->setData('default_shipping', $data['account']['default_shipping']);
            }
            if (isset($data['account']['confirmation'])) {
                $customer->setData('confirmation', $data['account']['confirmation']);
            }

            // Mark not modified customer addresses for delete
            foreach ($customer->getAddressesCollection() as $customerAddress) {
                if ($customerAddress->getId() && !in_array($customerAddress->getId(), $modifiedAddresses)) {
                    $customerAddress->setData('_deleted', true);
                }
            }

            if (Mage::getSingleton('admin/session')->isAllowed('customer/newsletter')) {
                $customer->setIsSubscribed(isset($data['subscription']));
            }

            if (isset($data['account']['sendemail_store_id'])) {
                $customer->setSendemailStoreId($data['account']['sendemail_store_id']);
            }

            $isNewCustomer = $customer->isObjectNew();
            try {
                $sendPassToEmail = false;
                // Force new customer confirmation
                if ($isNewCustomer) {
                    $customer->setPassword($data['account']['password']);
                    $customer->setForceConfirmed(true);
                    if ($customer->getPassword() == 'auto') {
                        $sendPassToEmail = true;
                        $customer->setPassword($customer->generatePassword());
                    }
                }

                Mage::dispatchEvent('adminhtml_customer_prepare_save', array(
                    'customer' => $customer,
                    'request' => $this->getRequest()
                ));

                $customer->save();


				 for ($b=0; $b < count($data['prescription']); $b++) {
					
				 	$bid=$data['prescription'][$b]['prescription_id'];
				 
					if($bid ) {
						$prescription = Mage::getModel('prescription/prescription')->load($bid);
						$prescription->setData('first_name', $data['prescription'][$b]['firstname']);
						$prescription->setData('customer_id', $data['customer_id']);
						$prescription->setData('middle_name', $data['prescription'][$b]['middlename']);
						$prescription->setData('last_name', $data['prescription'][$b]['lastname']);
						$prescription->setData('address', $data['prescription'][$b]['address']);
						$prescription->setData('email', $data['prescription'][$b]['email']);
						$prescription->setData('mobile', $data['prescription'][$b]['mobile']);
						$prescription->save();
					} else if($data['customer_id'] && $data['prescription'][$b]['firstname'] && $data['prescription'][$b]['email'] && $data['prescription'][$b]['mobile']) {
						$prescription = Mage::getModel('prescription/prescription');
						$prescription->setData('first_name', $data['prescription'][$b]['firstname']);
						$prescription->setData('customer_id', $data['customer_id']);
						$prescription->setData('middle_name', $data['prescription'][$b]['middlename']);
						$prescription->setData('last_name', $data['prescription'][$b]['lastname']);
						$prescription->setData('address', $data['prescription'][$b]['address']);
						$prescription->setData('email', $data['prescription'][$b]['email']);
						$prescription->setData('mobile', $data['prescription'][$b]['mobile']);
						$prescription->save();
					}
				}
				
//            print_r(json_encode($data)); exit;
                // Send welcome email
                if ($customer->getWebsiteId() && (isset($data['account']['sendemail']) || $sendPassToEmail)) {
                    $storeId = $customer->getSendemailStoreId();
                    if ($isNewCustomer) {
                        $customer->sendNewAccountEmail('registered', '', $storeId);
                    } elseif ((!$customer->getConfirmation())) {
                        // Confirm not confirmed customer
                        $customer->sendNewAccountEmail('confirmed', '', $storeId);
                    }
                }

                if (!empty($data['account']['new_password'])) {
                    $newPassword = $data['account']['new_password'];
                    if ($newPassword == 'auto') {
                        $newPassword = $customer->generatePassword();
                    }
                    $customer->changePassword($newPassword);
                    $customer->sendPasswordReminderEmail();
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('adminhtml')->__('The customer has been saved.')
                );
                Mage::dispatchEvent('adminhtml_customer_save_after', array(
                    'customer' => $customer,
                    'request' => $this->getRequest()
                ));

                if ($redirectBack) {
                    $this->_redirect('*/*/edit', array(
                        'id' => $customer->getId(),
                        '_current' => true
                    ));
                    return;
                }
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setCustomerData($data);
                $this->getResponse()->setRedirect($this->getUrl('*/customer/edit', array('id' => $customer->getId())));
            } catch (Exception $e) {
                $this->_getSession()->addException($e, Mage::helper('adminhtml')->__('An error occurred while saving the customer.'));
                $this->_getSession()->setCustomerData($data);
                $this->getResponse()->setRedirect($this->getUrl('*/customer/edit', array('id' => $customer->getId())));
                return;
            }
        }
        $this->getResponse()->setRedirect($this->getUrl('*/customer'));
    }
    
    public function saveCustomerAttachmentsAction()
    {
        try{
            
//$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
// 
//$select = $connection->select()
//    ->from('customer_prescription', array('*')) // select * from tablename or use array('id','title') selected values
//    ->where('customer_id=?',326);               // group by title
// 
//$rowsArray = $connection->fetchAll($select); // return all rows
//        echo "<pre>";
//        print_r($rowsArray);exit;
            //echo "<pre>";
            //print_r($_POST);exit;
            //echo "<pre>";
            //print_r($_FILES['myfile']['name']);exit;
            foreach($_POST['removepresc'] as $key => $data) {
               $delobjectModel = Mage::getModel('prescription/prescription');
               $delobjectModel->setId($key)->delete();
               $delobjectModel->unsetData();
            }
            $loop_counter = 0;
            //echo count($_POST['prescription_id']);exit;
            //for($i=0;$i<count($_POST['prescription_id'])-1;$i++) {
            foreach ($_FILES['myfile']['name'] as $key => $image) {
               /* if (empty($_FILES['myfile']['name'][$loop_counter])) {
                    continue;
                }*/
//                echo $i;
               $uploader = new Varien_File_Uploader(
                        array(
                                'name' => $_FILES['myfile']['name'][$loop_counter],
                                'type' => $_FILES['myfile']['type'][$loop_counter],
                                'tmp_name' => $_FILES['myfile']['tmp_name'][$loop_counter],
                                'error' => $_FILES['myfile']['error'][$loop_counter],
                                'size' => $_FILES['myfile']['size'][$loop_counter]
                        )
                );
//
//                // Allowed file extentions
                $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png','pdf','doc','docx','bmp','xls','xlsx'));
                $uploader->setAllowRenameFiles(true);

////                // We set media as the upload dir
                $path = Mage::getBaseDir('media') . DS . 'prescriptions' . DS;
               $img = $uploader->save($path, $_FILES['myfile']['name'][$loop_counter]);
                //$uploader->unsetData();
                //after uplad image you need add this line 
                //$customer = Mage::registry('current_customer');
//                $postData = array();
//                $postData['file']= "media". DS . 'prescriptions' . DS.str_replace(" ","_",$_FILES['myfile']['name'][$i]);
//                $postData['customer_id']=$_POST['customer_id'];
//                $postData['created_on']=date('Y-m-d h:i:s');
//                $postData['updated_on']=date('Y-m-d h:i:s');
//                $postData['comment']= $_POST['mycomment'][$i];
                //$postData['show']= 'Yes';
                //echo "<pre>";
                //print_r($postData);
                
                $pid = explode('_', $_POST['prescription_id'][$loop_counter]);
                //echo "<pre>";
                //print_r($pid);exit;
                //echo count($pid);exit;
                if(count($pid) == 1){
                    $delobjectModel = Mage::getModel('prescription/prescription');
                    $delobjectModel->setId($pid[0])->delete();
                    $delobjectModel->unsetData();
                    //$saveobjModel = Mage::getSingleton('prescription/prescription')->load($pid[0]);
                }
                
                //echo "<pre>";
                //print_r($saveobjModel);exit;
                $saveobjModel = Mage::getSingleton('prescription/prescription');
                $postData = array();
                $postData['file']= "media". DS . 'prescriptions' . DS.str_replace(" ","_",$_FILES['myfile']['name'][$loop_counter]);
                $postData['customer_id']=$_POST['customer_id'];
                $postData['created_on']=$_POST['updated_on'][$loop_counter];
                $postData['updated_on']=$_POST['updated_on'][$loop_counter];
                $postData['comment']= $_POST['mycomment'][$loop_counter];
                $saveobjModel->addData($postData)->save();//echo hi;exit;
                echo $saveobjModel->getId();
//                 $resource = Mage::getSingleton('core/resource');
//                 $writeConnection = $resource->getConnection('core_write');
                 //$table = $resource->getTableName('prescription/prescription');
//                 $customer_id = $_POST['customer_id'];
//                 $mycomment = $_POST['mycomment'][$i];
//                 $query = "INSERT INTO customer_prescription (customer_id, comment) VALUES ('$customer_id', '$mycomment')";
//     
//                 $writeConnection->query($query);                 
                 //echo hh;exit;   
                
                
//$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
// 
//$connection->beginTransaction();
// 
//$postData = array();
//$postData['file']= "media". DS . 'prescriptions' . DS.str_replace(" ","_",$_FILES['myfile']['name'][$i]);
//$postData['customer_id']=$_POST['customer_id'];
//$postData['created_on']=$_POST['updated_on'][$i];
//$postData['updated_on']=$_POST['updated_on'][$i];
//$postData['comment']= $_POST['mycomment'][$i];
//$connection->insert('customer_prescription', $postData);
// 
//$connection->commit();echo hi;exit;              
                
//                $saveobjModel->setFile("media". DS . 'prescriptions' . DS.str_replace(" ","_",$_FILES['myfile']['name'][$i]));
//                $saveobjModel->setCustomerId($_POST['customer_id']);
//                $saveobjModel->setCreatedOn($_POST['updated_on'][$i]);
//                $saveobjModel->setUpdatedOn($_POST['updated_on'][$i]);
//                $saveobjModel->setComment($_POST['mycomment'][$i]);
//                
//                $saveobjModel->save();echo hi;exit;
                //echo $saveobjModel->getId();
                $saveobjModel->unsetData();
                //unset($objectModel);
                //$msg = 'The attachments have been saved';
                //Mage::getSingleton('core/session')->addSuccess($msg);
                //$link.='<a href="'.Mage::getBaseUrl()."media". DS . 'prescriptions' . DS.str_replace(" ","_",$_FILES['myfile']['name'][$i]).'">'.$_FILES['myfile']['name'][$i].'</a><br>';
        $loop_counter = $loop_counter + 1;
                
                }//exit;
            
           //foreach($_POST['removepresc'] as $key => $data) {
           //    $objectModel = Mage::getModel('prescription/prescription');
           //    $objectModel->setId($key)->delete();
            //}
        //exit;    
          //  $this->_getSession()->addException($e,$msg);

        } catch (Exception $e) {
            $msg = Mage::helper('adminhtml')->__('An error occured:');
           //$this->_getSession()->addException($e,$msg);
        }

//        Mage::dispatchEvent('order_attachments_updated', array(
//            'order'             => Mage::getModel('sales/order')->load($orderId),
//            'order_attachments' => $orderAttachmentsCollection,
//            'updated_by'        => 'customer',
//        ));
        
        $msg = 'The attachments have been saved';
        Mage::getSingleton('core/session')->addSuccess($msg);
        
     //   $this->getResponse()->setRedirect($this->getUrl('*/customer/edit', array('id' => $_POST['customer_id'])));
    }
    
    /**
     * Upload a file and returs a string with the filename (+relative location)
     *
     * @throws Mage_Core_Exception
     * @param int $id
     * @return string
     */
    public function fileAttachmentsAction()
    {
        //$this->_initOrder();
        //$this->_initLayoutMessages('lensexpert_prescription/session');
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('lensexpert_prescription/adminhtml_customer_edit_tab_prescription')->toHtml()
        );
    }
    /**
     * Upload a file and returs a string with the filename (+relative location)
     *
     * @throws Mage_Core_Exception
     * @param int $id
     * @return string
     */
    public function uploadAttachmentAction()
    {
        $loop_counter = 0;
        $link='';
      // loop each file
        foreach ($_FILES['myfile']['name'] as $key => $image) {
            //echo $loop_counter;
           //continue if value is empty
            if (empty($image)) {
                continue;
            }
            try {
                /* Starting upload */
                // This is required 
                $uploader = new Varien_File_Uploader(
                        array(
                                'name' => $_FILES['myfile']['name'][$key],
                                'type' => $_FILES['myfile']['type'][$key],
                                'tmp_name' => $_FILES['myfile']['tmp_name'][$key],
                                'error' => $_FILES['myfile']['error'][$key],
                                'size' => $_FILES['myfile']['size'][$key]
                        )
                );

                // Allowed file extentions
                $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png','pdf','doc','docx','bmp','xls','xlsx'));
                $uploader->setAllowRenameFiles(true);

                // Set the file upload mode
                // false -> get the file directly in the specified folder
                // true -> get the file in the product like folders
                //  (file.jpg will go in something like /media/f/i/file.jpg)
                $file_path = $uploader->setFilesDispersion(false);
               // echo $_FILES['myfile']['tmp_name'][$key]; exit;
                

                // We set media as the upload dir
                $path = Mage::getBaseDir('media') . DS . 'prescriptions' . DS;
                $img = $uploader->save($path, $_FILES['myfile']['name'][$key]);
                $objectModel = Mage::getModel('prescription/prescription');
                //after uplad image you need add this line 
                $customer = Mage::getSingleton('customer/session')->getCustomer(); 
                $postData['file']= "media". DS . 'prescriptions' . DS.str_replace(" ","_",$_FILES['myfile']['name'][$key]);
                $postData['customer_id']=$customer->getId();
                $postData['created_on']=date('Y-m-d h:i:s');
                $postData['comment']= $_POST['customer_comment'][$loop_counter];
                $objectModel->addData($postData)->save();     
                $msg = 'The attachments have been saved';
                Mage::getSingleton('core/session')->addSuccess($msg);
                $link.='<a href="'.Mage::getBaseUrl()."media". DS . 'prescriptions' . DS.str_replace(" ","_",$_FILES['myfile']['name'][$key]).'">'.$_FILES['myfile']['name'][$key].'</a><br>';
            } catch (Exception $e) {
                $msg = 'There is problem in saving attachment';
                Mage::getSingleton('core/session')->addError($msg);
            }
            $loop_counter = $loop_counter + 1;
        }
        if($link!='')
        {
            /*
            *Date :19-Jan-2015
            * Author: Arpita Shah
            * Loads the html file named 'custom_email_template1.html' from
            * app/locale/en_US/template/email/activecodeline_custom_email1.html
            */ 
           $emailTemplate  = Mage::getModel('core/email_template')->loadDefault('customer_attachment_notification_email_template');									

           //Create an array of variables to assign to template
           $emailTemplateVariables = array();
           $emailTemplateVariables['link'] = $link;
           $emailTemplateVariables['customer_name'] = $customer->getFirstname()." ".$customer->getLastname();
          
           /**
            * The best part :)
            * Opens the activecodeline_custom_email1.html, throws in the variable array 
            * and returns the 'parsed' content that you can use as body of email
            */
           
           $customer = Mage::getSingleton('customer/session')->getCustomer(); 
           $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
           $emailTemplate->setSenderEmail($customer->getEmail());
           $emailTemplate->setSenderName($customer->getFirstname()." ".$customer->getLastname());
           $emailTemplate->setTemplateSubject('Customer Attachment Notification from '.$customer->getFirstname()." ".$customer->getLastname());
           /*
            * Or you can send the email directly, 
            * note getProcessedTemplate is called inside send()
            */
            try
            {   
                $emailTemplate->send(Mage::getStoreConfig('trans_email/ident_general/email'),Mage::getStoreConfig('trans_email/ident_general/name'), $emailTemplateVariables);
                $emailTemplate->send('arpita.vs@sigmainfo.net',Mage::getStoreConfig('trans_email/ident_general/name'), $emailTemplateVariables);
                
            }
            catch (Exception $e) 
            {
                 echo  $e->getMessage();
            }
        }
        $this->_redirect('customer/prescription');
        return;
    }
    
    public function uploadAttachment()
    {
        $loop_counter = 0;
        $link='';
      // loop each file
        echo "<pre>";
        print_r($_POST);exit;
        foreach ($_FILES as $key => $image) {
            //echo $loop_counter;
           //continue if value is empty
            if (empty($image)) {
                continue;
            }
            try {
                /* Starting upload */
                // This is required 
                $uploader = new Varien_File_Uploader(
                        array(
                                'name' => $_FILES['myfile']['name']['file'],
                                'type' => $_FILES['myfile']['type']['file'],
                                'tmp_name' => $_FILES['myfile']['tmp_name']['file'],
                                'error' => $_FILES['myfile']['error']['file'],
                                'size' => $_FILES['myfile']['size']['file']
                        )
                );

                // Allowed file extentions
                $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png','pdf','doc','docx','bmp','xls','xlsx'));
                $uploader->setAllowRenameFiles(true);

                // Set the file upload mode
                // false -> get the file directly in the specified folder
                // true -> get the file in the product like folders
                //  (file.jpg will go in something like /media/f/i/file.jpg)
                $file_path = $uploader->setFilesDispersion(false);
               // echo $_FILES['myfile']['tmp_name'][$key]; exit;
                

                // We set media as the upload dir
                $path = Mage::getBaseDir('media') . DS . 'prescriptions' . DS;
                $img = $uploader->save($path, $_FILES['myfile']['name']['file']);
                $objectModel = Mage::getModel('prescription/prescription');
                //after uplad image you need add this line 
                $customer = Mage::registry('current_customer');
                $postData['file']= "media". DS . 'prescriptions' . DS.str_replace(" ","_",$_FILES['myfile']['name']['file']);
                $postData['customer_id']=326;
                $postData['created_on']=date('Y-m-d h:i:s');
                $postData['comment']= $_POST['myfile']['comment'];
                $postData['show']= $_POST['myfile']['show'];
        //echo "<pre>";
        //print_r($postData);exit;
                $objectModel->addData($postData)->save();     
                $msg = 'The attachments have been saved';
                Mage::getSingleton('core/session')->addSuccess($msg);
                $link.='<a href="'.Mage::getBaseUrl()."media". DS . 'prescriptions' . DS.str_replace(" ","_",$_FILES['myfile']['name']['file']).'">'.$_FILES['myfile']['name']['file'].'</a><br>';
            } catch (Exception $e) {
                $msg = 'There is problem in saving attachment';
                Mage::getSingleton('core/session')->addError($msg);
            }
            $loop_counter = $loop_counter + 1;
        }
        $this->_redirect('customer/prescription');
        return;
    }    
    
  
  
   
}
