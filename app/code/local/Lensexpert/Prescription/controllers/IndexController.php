<?php 
/**
 * Lensexpert
 *
 * @category    Lensexpert
 * @package     Lensexpert_Prescription
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Lensexpert_Prescription_IndexController extends Mage_Core_Controller_Front_Action 
{

    /**
     * Initialize requested category object
     *
     * @return Mage_Catalog_Model_Category
     */
     public function getFieldName()
    {
        return 'order_attachment';
    }
    
    /**
     * Initialize requested category object
     *
     * @return Mage_Catalog_Model_Category
     */
    public function saveAttachmentsAction()
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer(); 
        $customerAttachmentsCollection = array();
        try{        
           // $customerId = $this->getRequest()->getParam('customer_id');
            
            $data = array();
            $i = 0;
            foreach ($_POST as $id => $postData) {
               // echo "<pre>"; print_r($_FILES); exit;
                
                // -------------- upload files
                $newFileName = '';

                // new record - ignore if no attachment
                if (!empty($_FILES[$id]['tmp_name']['file'])) {
                    $newFileName = $this->uploadAttachment();
                }

                // ---------------- set the model data and save

                $i++;
            }

            $msg = Mage::helper('lensexpert_prescription')->__('The attachments have been saved');
            $this->getSession()->addSuccess($msg);

        } catch (Exception $e) {
            $msg = Mage::helper('lensexpert_prescription')->__('An error occured: %s', $e->getMessage());
            $this->getSession()->addError($msg);
        }

        Mage::dispatchEvent('order_attachments_updated', array(
            'order'             => Mage::getModel('sales/order')->load($orderId),
            'order_attachments' => $orderAttachmentsCollection,
            'updated_by'        => 'customer',
        ));
        
        // display block
        $this->_initLayoutMessages('redpandaplus_orderattachments/session');
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('redpandaplus_orderattachments/customer_sales_order_view_attachments')
                ->setTemplate('redpandaplus_orderattachments/sales/order/view/manage_attachments_block.phtml')
                ->toHtml()
        );

        $this->_redirect('orderattachments/index/manageattachments', array('order_id' => $orderId));
        return;

    }
    
    /**
     * Upload a file and returs a string with the filename (+relative location)
     *
     * @throws Mage_Core_Exception
     * @param int $id
     * @return string
     */
    public function uploadAttachmentAction()
    {
        $loop_counter = 0;
        $link='';
      // loop each file
        foreach ($_FILES['myfile']['name'] as $key => $image) {
            //echo $loop_counter;
           //continue if value is empty
            if (empty($image)) {
                continue;
            }
            try {
                /* Starting upload */
                // This is required 
                $uploader = new Varien_File_Uploader(
                        array(
                                'name' => $_FILES['myfile']['name'][$key],
                                'type' => $_FILES['myfile']['type'][$key],
                                'tmp_name' => $_FILES['myfile']['tmp_name'][$key],
                                'error' => $_FILES['myfile']['error'][$key],
                                'size' => $_FILES['myfile']['size'][$key]
                        )
                );

                // Allowed file extentions
                $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png','pdf','doc','docx','bmp','xls','xlsx'));
                $uploader->setAllowRenameFiles(true);

                // Set the file upload mode
                // false -> get the file directly in the specified folder
                // true -> get the file in the product like folders
                //  (file.jpg will go in something like /media/f/i/file.jpg)
                $file_path = $uploader->setFilesDispersion(false);
              //print_r($_FILES); exit;
                

                // We set media as the upload dir
                $path = Mage::getBaseDir('media') . DS . 'prescriptions' . DS;

                /*echo $path;
                exit;*/
                $img = $uploader->save($path, $_FILES['myfile']['name'][$key]);
                $objectModel = Mage::getModel('prescription/prescription');
                //after uplad image you need add this line 
                $customer = Mage::getSingleton('customer/session')->getCustomer(); 
                $postData['file']= "media". DS . 'prescriptions' . DS.str_replace(" ","_",$_FILES['myfile']['name'][$key]);
                $postData['customer_id']=$customer->getId();
                $postData['created_on']=date('Y-m-d h:i:s');
                $postData['comment']= $_POST['customer_comment'][$loop_counter];
                $objectModel->addData($postData)->save();     
                $msg = 'The attachments have been saved';
                Mage::getSingleton('core/session')->addSuccess($msg);
                $link.='<a href="'.Mage::getBaseUrl()."media". DS . 'prescriptions' . DS.str_replace(" ","_",$_FILES['myfile']['name'][$key]).'">'.$_FILES['myfile']['name'][$key].'</a><br>';
            } catch (Exception $e) {
                $msg = 'There is problem in saving attachment';
                Mage::getSingleton('core/session')->addError($msg);
            }
            $loop_counter = $loop_counter + 1;
        }
        if($link!='')
        {
            /*
            *Date :19-Jan-2015
            * Author: Arpita Shah
            * Loads the html file named 'custom_email_template1.html' from
            * app/locale/en_US/template/email/activecodeline_custom_email1.html
            */ 
           $emailTemplate  = Mage::getModel('core/email_template')->loadDefault('customer_attachment_notification_email_template');									

           //Create an array of variables to assign to template
           $emailTemplateVariables = array();
           $emailTemplateVariables['link'] = $link;
           $emailTemplateVariables['customer_name'] = $customer->getFirstname()." ".$customer->getLastname();
          
           /**
            * The best part :)
            * Opens the activecodeline_custom_email1.html, throws in the variable array 
            * and returns the 'parsed' content that you can use as body of email
            */
           
           $customer = Mage::getSingleton('customer/session')->getCustomer(); 
           $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
           $emailTemplate->setSenderEmail($customer->getEmail());
           $emailTemplate->setSenderName($customer->getFirstname()." ".$customer->getLastname());
           $emailTemplate->setTemplateSubject('Customer Attachment Notification from '.$customer->getFirstname()." ".$customer->getLastname());
           /*
            * Or you can send the email directly, 
            * note getProcessedTemplate is called inside send()
            */
            try
            {   
                $emailTemplate->send(Mage::getStoreConfig('trans_email/ident_general/email'),Mage::getStoreConfig('trans_email/ident_general/name'), $emailTemplateVariables);
                $emailTemplate->send('arpita.vs@sigmainfo.net',Mage::getStoreConfig('trans_email/ident_general/name'), $emailTemplateVariables);
                
            }
            catch (Exception $e) 
            {
                 echo  $e->getMessage();
            }
        }
        $this->_redirect('customer/prescription');
        return;
    }
    
     
}
