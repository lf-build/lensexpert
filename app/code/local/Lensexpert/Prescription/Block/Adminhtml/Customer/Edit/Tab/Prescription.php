<?php
/**
 * Lensexpert
 *
 * @category    Lensexpert
 * @package     Lensexpert_Prescription
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Lensexpert_Prescription_Block_Adminhtml_Customer_Edit_Tab_Prescription
 extends Mage_Adminhtml_Block_Template
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    public function __construct()
    {
        $this->setTemplate('prescription/prescription.phtml');
    }
	
	public function getCustomerAttachments()
    {
        $customer_id = $this->getRequest()->getParam('id');
		$customerAttachments = Mage::getModel('prescription/prescription')
                                ->getCustomerAttachments($customer_id);

        $files = array();
 $ite_file=array();
 $ite_file=array();
        if (!empty($customerAttachments['items'])) {
            foreach ($customerAttachments['items'] as $item) 
                {
                if(!in_array($item['file'],  $ite_file) && !in_array($item['created_on'],  $ite_time))
                {
                 $ite_file[]=$item['file'];
                 $ite_time[]=$item['created_on'];
                 $files[] = array(
                    'id' => $item['prescription_id'],
					'customer_id' => $item['customer_id'],
                    'comment' => $this->htmlEscape($item['comment']),
					'file' => $item['file'],
                    'file_name' =>  basename($item['file']),
                    'url' => "http://lensexperts.com/media/prescriptions/".basename($item['file']),
	            'created_on' => $item['created_on'],
                    'updated_on' => $item['updated_on'],
                );
                }
            }
        }

        return $files;
    }

    public function getCustomtabInfo(){

        $customer_id = $this->getRequest()->getParam('id');
        $prescription = Mage::getModel('prescription/prescription')->getCollection()->addOrder('prescription_id', 'DESC');
        $prescription->addFieldToFilter('customer_id', array('eq' => $customer_id));
        return $prescription;
		}

    /**
     * Return Tab label
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Customer Prescription');
    }

    /**
     * Return Tab title
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('Prescription');
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        $customer = Mage::registry('current_customer');
        return (bool)$customer->getId();
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }

     /**
     * Defines after which tab, this tab should be rendered
     *
     * @return string
     */
    public function getAfter()
    {
        return 'tags';
    }
    
    /**
     * Defines after which tab, this tab should be rendered
     *
     * @return string
     */
    protected function _prepareLayout()
    {

        $button = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
                'label'   => Mage::helper('prescription')->__('Save Attachments Section'),
                'class'   => 'save',
                'onclick' =>  "document.getElementById('file_upload_form').action = '".$this->getUrl('*/*/saveCustomerAttachments')."';document.getElementById('file_upload_form').submit();",
                'id' => 'save-attachments-btn',
            ));
        $this->setChild('submit_button', $button);


        return parent::_prepareLayout();
    }
    
    /**
     * Defines after which tab, this tab should be rendered
     *
     * @return string
     */
    public function getSubmitUrl()
    {
      $url = Mage::getUrl('*/customer/fileAttachments');
        return $url;
    }
     public function getHtmlId()
    {
        return 'customer-attachment';
    }
    
//     public function getTabUrl()
//    {
//        return $this->getUrl('adminhtml/customer/uploadAttachment', array('_current' => true));
//    }
    
}
?>

