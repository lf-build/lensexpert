<?php
/**
 * Lensexpert
 *
 * @category    Lensexpert
 * @package     Lensexpert_Prescription
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Lensexpert_Prescription_Block_Customer extends Mage_Customer_Block_Account_Dashboard
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('customer/prescription.phtml');
        $customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
        $prescription = Mage::getModel('prescription/prescription')->getCollection()
            ->addFieldToFilter('customer_id', array('eq' => $customer_id))
            ->setOrder('prescription_id', 'desc')
        ;
        $this->setPrescription($prescription);

    }
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock('page/html_pager', 'sales.order.history.pager')
            ->setCollection($this->getPrescription());
        $this->setChild('pager', $pager);
        $this->getPrescription()->load();
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
    
    public function getSaveUrl()
    {
        return Mage::getUrl('prescription/index/uploadAttachment',array('_secure'=>true));
    }
    
    public function getFieldName()
    {
        return 'customer_attachment';
    }
    
    public function getFieldId()
    {
        return 'customer-attachment';
    }
    
     public function getCustomerAttachments()
    {   
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        //echo  $customer->getId();
        $collection = Mage::getModel('prescription/prescription')->getCollection()
                        ->addFieldToFilter('customer_id', $customer->getId());

        return $collection;
    }
    
}
