<?php
/**
 * Lensexpert
 *
 * @category    Lensexpert
 * @package     Lensexpert_Prescription
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Lensexpert_Prescription_Model_Mysql4_Prescription extends Mage_Core_Model_Mysql4_Abstract
{
     public function _construct()
     {
         $this->_init('prescription/prescription', 'prescription_id');
     }
}

