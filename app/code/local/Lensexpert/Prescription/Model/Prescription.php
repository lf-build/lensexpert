<?php
/**
 * Lensexpert
 *
 * @category    Lensexpert
 * @package     Lensexpert_Prescription
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Lensexpert_Prescription_Model_Prescription extends Mage_Core_Model_Abstract
{
     public function _construct()
     {
         parent::_construct();
         $this->_init('prescription/prescription');
     }
       public function getCustomerAttachments($customer_id)
    {   
        $collection = Mage::getModel('prescription/prescription')->getCollection()
                        ->addFieldToFilter('customer_id', $customer_id);

        return $collection->toArray();
    }
}