<?php

class Magestore_Onestepcheckout_Model_Customer_Customer extends Mage_Customer_Model_Customer
{    

    /**
     * Validate customer attribute values.
     * For existing customer password + confirmation will be validated only when password is set (i.e. its change is requested)
     *
     * @return bool
     */
    public function validate()
    {
        if(Mage::helper('onestepcheckout')->enabledOnestepcheckout()){
			return true;
		}	
		
		return parent::validate();
    }
      /**
     * Processing object before save data
     *
     * @return Mage_Customer_Model_Customer
     */
    protected function _beforeSave()
    {	 Mage::log('46', null, 'fmedata.log');
        if(Mage::helper('fieldsmanager')->getStoredDatafor('enable')){
	    Mage::getSingleton('customer/session')->setRegistry('');
	    $data=array();$fmedata=array();
	    if(isset($_POST['fme_register'])){
		$fmedata=$_POST['fme_register'];
	    }elseif(isset($_POST['fme_account'])){
		$fmedata=$_POST['fme_account'];
	    }
	    foreach($fmedata as $key=>$value){
	      if(substr($key,0,3)=='fm_'){
		$data[substr($key,3)]=$value;
		}
	    }
	    if(is_array($data) and count($data)!=0){
		Mage::getSingleton('customer/session')->setRegistry($data);
	    }
	}
        return  parent::_beforeSave();
    }

}
