<?php
require_once 'Mage/Checkout/controllers/OnepageController.php';
class Lens_Custom_OnepageController extends Mage_Checkout_OnepageController
{
   public function successAction() {
        $session = $this->getOnepage()->getCheckout();
        if (!$session->getLastSuccessQuoteId()) {
            $this->_redirect('checkout/cart');
            return;
        }
       //  Mage::log('Check2',null,'checkoutdata.log');
        $lastQuoteId = $session->getLastQuoteId();
        $lastOrderId = $session->getLastOrderId();
        $lastRecurringProfiles = $session->getLastRecurringProfileIds();
        if (!$lastQuoteId || (!$lastOrderId && empty($lastRecurringProfiles))) {
            $this->_redirect('checkout/cart');
            return;
        }
        if($lastOrderId)
        {
            $observerModel = Mage::getModel('supplier/observer');
            $dd = $observerModel->manual($lastOrderId);
        }
        
        $session->clear();
        $this->loadLayout();
        $this->_initLayoutMessages('checkout/session');
        Mage::dispatchEvent('checkout_onepage_controller_success_action', array('order_ids' => array($lastOrderId)));
        $this->renderLayout();
        //echo "hurray"; exit;
    }
}

